DROP TABLE IF EXISTS [Projects];
CREATE TABLE `Projects` (`title` VARCHAR(255), `description` TEXT, `id` INTEGER PRIMARY KEY AUTOINCREMENT, `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL);
DROP TABLE IF EXISTS [ProjectsTasks];
CREATE TABLE `ProjectsTasks` (`TaskId` INTEGER NOT NULL, `ProjectId` INTEGER NOT NULL, `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, PRIMARY KEY (`TaskId`, `ProjectId`));
DROP TABLE IF EXISTS [Tasks];
CREATE TABLE `Tasks` (`title` VARCHAR(255), `description` TEXT, `deadline` DATETIME, `id` INTEGER PRIMARY KEY AUTOINCREMENT, `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL);
