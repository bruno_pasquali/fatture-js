'use sctrict;'

var url = require('url');
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('f.sqlite');
var fs = require('fs');

routeTable = {
    '/': index,
    '/fatture': fatture,
    '/clienti': clienti
}
//db.serialize();
//db.run("PRAGMA foreign_keys = ON;")

function getTableData(table, callback, error){
  db.all("SELECT * FROM " + table + " LIMIT 600", function(err, rows) {
  //db.all("SELECT * FROM Cliente LIMIT 100", function(err, rows) {
  //db.all("SELECT * FROM StoricoFattureEstintori LIMIT 10", function(err, rows) {
    console.log("table: " + table);
    var o = {table: table, rows: rows};
    //console.log(o);
    callback(JSON.stringify(o), 'text/json');
  });  
}
function fatture(route, callback, error){
  getTableData('StoricoFattureEstintori', callback, error);
}
function clienti(route, callback, error){
  getTableData('Cliente', callback, error);
}
function index(route, callback, error) {
  fs.exists('index.html', function (exists) {
    if (!exists) error('File not found');
  });  
  fs.readFile('index.html', function (err, data) {
    if (err) error('File read error');
    else callback(data, 'text/html');
  });    
}

function getData(route, callback, error) {
  console.log(route);
  action = routeTable[route];
  if (action) action(route, callback, error);
  else error('Action not supported');
}

var http = require('http');
http.createServer(function (req, res) {
    getData(url.parse(req.url).pathname, function(response, type){
        var u = url.parse(req.url);
        console.log(decodeURI(u.path));
        console.log(u);
        res.writeHead(200, {'Content-Type': type});
        res.end(response); 
    }, function(message) {
        res.writeHead(401, message);
        res.end(message); 
    });
}).listen(1337, '127.0.0.1');

console.log('Server running at http://127.0.0.1:1337/');

process.on('SIGINT', function() {
    db.close();
    console.log('Quitting');
    process.exit(1);
});