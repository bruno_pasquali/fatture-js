var Sequelize = require('sequelize'),
    fs = require('fs'),
    util = require('util'),
    path = require('path');

var options = {
    dialect: 'sqlite',
    // logging: false,
    storage: path.join('test.sqlite3'),
	define: {
	    instanceMethods: {
	        toPlainObject: function(readOnly) {
	            return this.injectModelInto(JSON.parse(JSON.stringify(this)), readOnly);
	        },
	        injectModelInto: function(obj, readOnly) {
	            var self = this;
	            obj._bag = {
	                model: self.daoFactoryName,
	                readOnly: Boolean(readOnly)
	            };
	            for (var p in self.dataValues) {
	                if (util.isArray(self.dataValues[p])) {
	                    var i = 0,
	                        l = self.dataValues[p].length;
	                    for (; i < l; i++) {
	                        self.dataValues[p][i].injectModelInto(obj[p][i], readOnly);
	                    }

	                } else if ((self.dataValues[p] === Object(self.dataValues[p]) &&
	                    (self.dataValues[p]['dataValues']))) {
	                    self.dataValues[p].injectModelInto(obj[p], readOnly);
	                }
	            }
	            return obj;
	        }
	    }
	}
};

var sequelize = new Sequelize('db', 'username', 'password', options);

var Project = sequelize.define('Project', {
  title: Sequelize.STRING,
  description: Sequelize.TEXT
}, {freezeTableName: true});

var Task = sequelize.define('Task', {
  title: Sequelize.STRING,
  description: Sequelize.TEXT,
  deadline: Sequelize.DATE
}, {freezeTableName: true});

// Project.hasOne(Task, {onDelete: 'cascade'});
// Task.belongsTo(Project, {onDelete: 'cascade'});
Project.hasMany(Task, {onDelete: 'cascade'});
Task.hasMany(Project, {onDelete: 'cascade'});

var t = Task.build({title: "aaa"});
console.dir(t.values);
console.dir(t.toPlainObject());

if (false) {
sequelize.sync({force: true}).success(function() {

	Project.create({ title: 'prj', description: 'project'}).success(function(project) {
		Task.create({ title: 'foo1', description: 'bar1', deadline: new Date() }).success(function(task1) {
			Task.create({ title: 'foo2', description: 'bar2', deadline: new Date() }).success(function(task2) {
				project.setTasks([task1, task2]).success(function(tasks) {
				// project.setTasks([]).success(function(tasks) {
					// project.getTasks().success(function(associatedTask) {
		   //              console.log(associatedTask)
					// });
/*
					tasks[0].destroy().success(function(){
						project.setTasks(null).success(function() {
						// Task.findAll().success(function(tasks){
							tasks[1].destroy().success(function(){
								Task.findAll().ok(function(ts){
				            	    // console.dir(ps);
					                console.dir(ts)
				            	});
							});
						});
					});

*/

					// ok now they are save... how do I get them later on?
					project.getTasks().success(function(associatedTasks) {
					  	// associatedTasks is an array of tasks
		                console.log(associatedTasks[0].values)
	                    Task.find({where: {title: 'foo1'}}).success(function(task){
	                    // Task.find({where: {title: 'foo1'}}).success(function(task){
	                        task.description = 'changed desc';
	                        task.save();        
	                    });
					});

				});
			});	
		});	
	});	
});
}