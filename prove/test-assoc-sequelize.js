var Sequelize = require('sequelize'),
    fs = require('fs'),
    util = require('util'),
    path = require('path');

var options = {
    dialect: 'sqlite',
    storage: path.join('test.sqlite3')
};

var sequelize = new Sequelize('db', 'username', 'password', options);

var Left = sequelize.define('Left', {
  title: Sequelize.STRING,
  description: Sequelize.TEXT
});

var Rigth = sequelize.define('Rigth', {
  title: Sequelize.STRING,
  description: Sequelize.TEXT
});

var LeftsRigths = sequelize.define('LeftsRigths', {
  note: Sequelize.STRING
});

// Left.hasOne(LeftsRigths, {foreignKeyConstraint: true});
Rigth.hasOne(LeftsRigths, {foreignKeyConstraint: true});
// LeftsRigths.belongsTo(Left, {foreignKeyConstraint: true});
LeftsRigths.belongsTo(Rigth, {foreignKeyConstraint: true});

Left.hasMany(Rigth, {foreignKeyConstraint: true, through: LeftsRigths });
Rigth.hasMany(Left, {foreignKeyConstraint: true, through: LeftsRigths });

sequelize.sync({force: true}).success(function() {
});
