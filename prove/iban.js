function IBANChk(b) 
{ 
        if (b.length < 5) { alert("La lunghezza � minore di 5 caratteri"); return false; } 
        s = b.substring(4) + b.substring(0, 4); 
        for (i = 0, r = 0; i < s.length; i++ ) 
        { 
                c = s.charCodeAt(i); 
                if (48 <= c && c <= 57) 
                { 
                        if (i == s.length-4 || i == s.length-3) { alert("Posizioni 1 e 2 non possono contenere cifre"); return false; } 
                        k = c - 48; 
                } 
                else if (65 <= c && c <= 90) 
                { 
                        if (i == s.length-2 || i == s.length-1) { alert("Posizioni 3 e 4 non possono contenere lettere"); return false; } 
                        k = c - 55; 
                } 
                else { alert("Sono ammesse solo cifre e lettere maiuscole"); return false; } 
                if (k > 9) 
                        r = (100 * r + k) % 97; 
                else 
                        r = (10 * r + k) % 97; 
        } 
        if (r != 1) { alert("Il codice di controllo � errato"); return false; } 
        alert("L'IBAN risulta corretto"); 
        return true; 
} 