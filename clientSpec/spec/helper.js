beforeEach(function () {
    var options = {
        path: 'http://localhost:3001'
    };
    var xmlhttp=new XMLHttpRequest();

  jasmine.addMatchers({
    toBePlaying: function () {
      return {
        compare: function (actual, expected) {
          var player = actual;

          return {
            pass: player.currentlyPlayingSong === expected && player.isPlaying
          }
        }
      };
    }
  });
});
