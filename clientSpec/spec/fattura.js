describe("Fattura", function() {
    var fattura, cliente, idAbbonamento, abbonamento;
    var options = {
        path: 'http://localhost:3001'
    };
    var xmlhttp = new XMLHttpRequest();

    beforeEach(function() {});

    it("should be able to get a customer", function() {
        var id = '12';
        xmlhttp.open("GET", options.path + '/r/clienti/' + id, false);
        xmlhttp.send();
        expect(JSON.parse(xmlhttp.responseText).code.toString()).toEqual('200');
        var data = JSON.parse(xmlhttp.responseText);
        cliente = data.data;
        idAbbonamento = cliente.abbonamentoEstintori[0].id;
        expect(cliente.id.toString()).toEqual(id);
    });

    it("should be able to get a subscription", function() {
        xmlhttp.open("GET", options.path + '/r/abbonamenti/' + idAbbonamento, false);
        xmlhttp.send();
        expect(JSON.parse(xmlhttp.responseText).code.toString()).toEqual('200');
        var data = JSON.parse(xmlhttp.responseText);
        abbonamento = data.data;
        expect(abbonamento.id).toEqual(idAbbonamento);
    });

    it("Creates an invoice from customer a subscription", function() {
        fattura = new myScope.Fattura({cliente: cliente, abbonamento: abbonamento});
        expect(fattura.indirizzo).toEqual(cliente.indirizzo.indirizzo);
        expect(fattura.comune).toEqual(cliente.indirizzo.comune.comune);
    });

    it("Saves to the server an invoice from a customer subscription", function() {
        fattura = new myScope.Fattura({cliente: cliente, abbonamento: abbonamento});
        xmlhttp.open("POST", options.path + '/r/StoricoFattureEstintori/', false);
        xmlhttp.setRequestHeader("Content-type", "application/json; charset=utf-8");
        xmlhttp.send(JSON.stringify(fattura));
        newFattura = JSON.parse(xmlhttp.responseText).data;
        expect(newFattura.indirizzo).toEqual(fattura.indirizzo);
    });

});

/*
{
    id: 4,
    ClienteId: 4,
    AbbonamentoId: 4,
    anno: 2014,
    numero: 4,
    numeroPrecedente: 4,
    ragioneSociale: "Besenzoni Legnami srl ",
    codiceFiscale: "00391970167",
    partitaIVA: "00391970167",
    indirizzo: "Via Camozzi, 20 ",
    localita: "",
    comune: "Villongo",
    CAP: "24060",
    provincia: "BG",
    indirizzoSpedizione: "Via Torquato Tasso ",
    localitaSpedizione: "",
    comuneSpedizione: "Villongo",
    CAPSpedizione: "24060",
    provinciaSpedizione: "BG",
    indirizzoManutenzione: " ",
    localitaManutenzione: "",
    comuneManutenzione: "",
    CAPManutenzione: "",
    provinciaManutenzione: "",
    note: " ",
    descrizionePagamento: "DF.FM.",
    costo: 88,
    numeroEstintori: 5,
    IVA: 22,
    descrizioneSpese: "RIBA",
    costoSpese: 4,
    dataEmissione: "2014-01-02",
    dataPagamento: 0,
    dataFatturaFineMese: true,
    seNotaAccredito: false,
    seDisdetta: false,
    giorniPagamento: 60,
    tipoPagamento: 201,
    ritenuta: 0,
    ABI: "03069",
    CAB: "53470",
    banca: "Banca INTESA",
    filiale: "Sarnico",
    contoCorrente: "",
    IBAN: "",
    createdAt: "2014-12-30T19:13:15.748Z",
    updatedAt: "2014-12-30T19:13:15.748Z",
    _bag: {
        model: "StoricoFattureEstintori",
        readOnly: false
    }

    {
        "id": 3,
        "numero": 3,
        "ragioneSociale": "VOGLIA di PIZZA snc di Bertazzoli Gaia & C.",
        "codiceFiscale": "BRTGAI74H68I437Q",
        "partitaIVA": "02343090169",
        "note": " ",
        "createdAt": "2014-12-30T19:13:15.748Z",
        "updatedAt": "2015-01-05T20:44:02.000Z",
        "IndirizzoFatturaId": null,
        "IndirizzoId": 5,
        "contatto": [],
        "contoCorrente": [],
        "indirizzo": {
            "id": 5,
            "indirizzo": "Via S. Pancrazio, 27 ",
            "localita": "",
            "note": "",
            "CAP": "25036",
            "createdAt": "2014-12-30T19:13:15.748Z",
            "updatedAt": "2015-01-05T20:44:02.000Z",
            "ComuneId": 4928,
            "comune": {
                "id": 4928,
                "istat": 17133,
                "comune": "Palazzolo sull'Oglio",
                "CAP": "25036",
                "provincia": "BS",
                "regione": "LOM",
                "prefisso": "030",
                "codiceFisco": "G264",
                "createdAt": "2014-12-30 20:13:15.748285",
                "updatedAt": "2015-01-05 20:44:02.000 +00:00",
                "_bag": {
                    "model": "Comune",
                    "readOnly": false
                }
            },
            "_bag": {
                "model": "Indirizzo",
                "readOnly": false
            }
        },
        "indirizzoFattura": null,
        "abbonamentoEstintori": [{
            "id": 3,
            "numero": 3,
            "descrizionePagamento": "DF.FM.",
            "note": " ",
            "costo": 43,
            "numeroEstintori": 1,
            "IVA": 22,
            "descrizioneSpese": "",
            "costoSpese": 0,
            "seDisdetta": false,
            "giornoEmissione": 2,
            "meseEmissione": 1,
            "giorniPagamento": 60,
            "seFatturareAGennaio": true,
            "dataFatturaFineMese": true,
            "ritenuta": 0,
            "tipoPagamento": 202,
            "tipo": 0,
            "createdAt": "2014-12-30T19:13:15.748Z",
            "updatedAt": "2015-01-05T20:44:02.000Z",
            "ClienteId": 3,
            "IndirizzoId": 5,
            "contatto": [],
            "indirizzo": {
                "id": 5,
                "indirizzo": "Via S. Pancrazio, 27 ",
                "localita": "",
                "note": "",
                "CAP": "25036",
                "createdAt": "2014-12-30T19:13:15.748Z",
                "updatedAt": "2015-01-05T20:44:02.000Z",
                "ComuneId": 4928,
                "comune": {
                    "id": 4928,
                    "istat": 17133,
                    "comune": "Palazzolo sull'Oglio",
                    "CAP": "25036",
                    "provincia": "BS",
                    "regione": "LOM",
                    "prefisso": "030",
                    "codiceFisco": "G264",
                    "createdAt": "2014-12-30 20:13:15.748285",
                    "updatedAt": "2015-01-05 20:44:02.000 +00:00",
                    "_bag": {
                        "model": "Comune",
                        "readOnly": false
                    }
                },
                "_bag": {
                    "model": "Indirizzo",
                    "readOnly": false
                }
            },
            "_bag": {
                "model": "AbbonamentoEstintori",
                "readOnly": false
            }
        }, {
            "id": 706,
            "numero": 655,
            "descrizionePagamento": "",
            "note": "",
            "costo": 0,
            "numeroEstintori": 0,
            "IVA": 0,
            "descrizioneSpese": "",
            "costoSpese": 0,
            "seDisdetta": false,
            "giornoEmissione": 1,
            "meseEmissione": 1,
            "giorniPagamento": 0,
            "seFatturareAGennaio": false,
            "dataFatturaFineMese": true,
            "ritenuta": 0,
            "tipoPagamento": 0,
            "tipo": 0,
            "createdAt": "2015-01-05T20:43:38.000Z",
            "updatedAt": "2015-01-05T20:44:02.000Z",
            "ClienteId": 3,
            "IndirizzoId": null,
            "contatto": [],
            "indirizzo": null,
            "_bag": {
                "model": "AbbonamentoEstintori",
                "readOnly": false
            }
        }],
        "_bag": {
            "model": "AbbonamentoEstintori",
            "readOnly": false
        },
        "descrizionePagamento": "DF.FM.",
        "costo": 43,
        "numeroEstintori": 1,
        "IVA": 22,
        "descrizioneSpese": "",
        "costoSpese": 0,
        "seDisdetta": false,
        "giornoEmissione": 2,
        "meseEmissione": 1,
        "giorniPagamento": 60,
        "seFatturareAGennaio": true,
        "dataFatturaFineMese": true,
        "ritenuta": 0,
        "tipoPagamento": 202,
        "tipo": 0,
        "ClienteId": 3
    }
*/
