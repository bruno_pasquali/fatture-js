/*
  See: 
  http://www.geedew.com/2012/10/24/remove-a-directory-that-is-not-empty-in-nodejs/
*/
var fs = require('fs');

var deleteFolderRecursive = function(path) {
  if( fs.existsSync(path) ) {
    fs.readdirSync(path).forEach(function(file,index){
      var curPath = path + "/" + file;
      if(fs.statSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        console.log('f:'+curPath);  
        fs.unlinkSync(curPath);
      }
    });
    console.log('d:'+path);  
    fs.rmdirSync(path);
  }
};

//deleteFolderRecursive('.');