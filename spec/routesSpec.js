describe('Api REST', function() {

    var path = require('path');
    var fs = require('fs');
    var util = require('util');
    var sqlite3 = require('sqlite3');
    var db = new sqlite3.Database('server/db/test.sqlite3');
    console.log(__dirname);
    var myScope = require('../server/public/js/my');
    //var a = new myScope.Abbonamento();
    console.dir(myScope);
    var inspectOpts = {
        showHidden: false,
        depth: 6 //,
        // colors: true
    };
    var log = function(data) {
        fs.appendFileSync('logs/routeSpec.log', data + '\n');
    };
    var dlog = function(data) {
        fs.appendFileSync('logs/routeSpec.log', util.inspect(data, inspectOpts) + '\n');
    };

    var http = require('http');
    var options = {
        hostname: 'localhost',
        port: 3002,
    };

    var that = {};

    var restAction = function(action, path, item, done, expectation) {
        options.path = '/r/' + path;
        if (action === 'LIST') {
            options.path += '?limit=100';
            action = 'GET';
        }
        options.method = action;
        var data = '';
        var req = http.request(options, function(res) {
            log('ACTION: ' + action);
            log('STATUS: ' + res.statusCode);
            // expect(res.statusCode).toBe(200);
            log('HEADERS: ' + JSON.stringify(res.headers));
            res.setEncoding('utf8');
            res.on('data', function(chunk) {
                data += chunk;
            });
            res.on('end', function() {
                log('BODY: ' + data);
                if ((!res.headers['content-type']) || (res.headers['content-type'].indexOf('application/json') === -1)) {
                    expectation(data, res);
                } else {
                    expectation(JSON.parse(data), res);
                }
                done();
            });
            res.on('timeout', function(chunk) {
                expect(false).echoIfFalse('Connection timeout');
                done();
            });
        });
        req.on('error', function(e) {
            var message = 'problem with request: ' + e.message;
            expect(false).echoIfFalse(message);
            done();
        });
        if (item) {
            log('sending data: ' + JSON.stringify(item));
            req.setHeader("Content-Type", "application/json; charset=utf-8");
            req.write(JSON.stringify(item));
        }
        req.end();
    };

    var testEmpty = function (table, done){
      db.all("SELECT * FROM " + table + " LIMIT 1", function(err, rows) {
        expect(rows.length === 0).echoIfFalse(table + ' non é vuota');
        done();
      });
    }

    beforeEach(function() {
        this.addMatchers({
            echoIfFalse: function(expected) {
                this.message = function() {
                   return expected;
                };
                return this.actual;
            }
        });
    });

    it('Svuota il database di test', function(done) {
        restAction('DELETE', 'all', null, done, function(data, res) {
            expect(res.statusCode).toBe(204);
        });
    });
    it('Crea un AbbonamentoEstintori isolato ..', function(done) {
        restAction('POST', 'AbbonamentoEstintori', null, done, function(data, res) {
            expect(res.statusCode).toBe(201);
            that.abbonamento = data.data;
            expect(that.abbonamento.id).toBeTruthy();
        });
    });
    it('.. lo aggiorna ..', function(done) {
        that.abbonamento.note = 'nota';
        restAction('PUT', 'AbbonamentoEstintori', that.abbonamento, done, function(data, res) {
            expect(res.statusCode).toBe(200);
            expect(data.data.note).toBeTruthy('nota');
        });
    });
    it('.. lo rilegge ..', function(done) {
        restAction('GET', 'AbbonamentoEstintori/' + that.abbonamento.id, null, done, function(data, res) {
            expect(res.statusCode).toBe(200);
            expect(data.data.note).toBeTruthy('nota');
        });
    });
    it('   .. e lo cancella', function(done) {
        restAction('DELETE', 'AbbonamentoEstintori/' + that.abbonamento.id, null, done, function(data, res) {
            expect(res.statusCode).toBe(204);
        });
    });
    it('Creo un Cliente', function(done) {
        restAction('POST', 'Cliente', null, done, function(data, res) {
            expect(res.statusCode).toBe(201);
            that.primoCliente = data.data;
            expect(that.primoCliente.id).toBeTruthy();
            expect(that.primoCliente.numero).toBeTruthy();
        });
    });
    it('Creo un secondo Cliente', function(done) {
        restAction('POST', 'Cliente', null, done, function(data, res) {
            expect(res.statusCode).toBe(201);
            that.cliente = data.data;
            expect(that.cliente.id).toBeTruthy();
            expect(that.cliente.numero).toBeTruthy();
        });
    });
    it('Elenco tabella "Cliente"', function(done) {
        restAction('LIST', 'Cliente', null, done, function(data, res) {
            expect(res.statusCode).toBe(200);
            expect((data.data.length)).toBe(2);
        });
    });
    it('Crea un AbbonamentoEstintori per il secondo cliente', function(done) {
        restAction('POST', 'Cliente/'+that.cliente.id+'/AbbonamentoEstintori', null, done, function(data, res) {
        // dlog(data.data);
            expect(res.statusCode).toBe(201);
            that.abbonamento = data.data;
            expect(that.abbonamento.id).toBeTruthy();
            expect(that.abbonamento.IndirizzoId).toBeFalsy();
            expect(that.abbonamento.numero).toBeGreaterThan(0);
        });
    });
    it('Aggiungo un indirizzo per invio fattura', function(done) {
        restAction('POST', 'Cliente/' + that.cliente.id +'/Indirizzo.IndirizzoFattura', null, done, function(data, res) {
            expect(res.statusCode).toBe(201);
        });
    });
    it('Aggiungo un indirizzo', function(done) {
        restAction('POST', 'Cliente/' + that.cliente.id +'/Indirizzo', null, done, function(data, res) {
            expect(res.statusCode).toBe(201);
        });
    });
    it('Aggiungo un contatto', function(done) {
        var contatto = {contatto: 'abc.12345'};
        restAction('POST', 'Cliente/' + that.cliente.id +'/Contatto', contatto, done, function(data, res) {
            expect(res.statusCode).toBe(201);
        });
    });
    it('Aggiungo un conto corrente', function(done) {
        restAction('POST', 'Cliente/' + that.cliente.id +'/ContoCorrente', null, done, function(data, res) {
            expect(res.statusCode).toBe(201);
        });
    });
    it('Aggiungo un indirizzo all\'abbonamento del cliente', function(done) {
        restAction('POST', 'AbbonamentoEstintori/' + that.abbonamento.id +'/Indirizzo', null, done, function(data, res) {
            expect(res.statusCode).toBe(201);
        });
    });
    it('Aggiungo un contatto all\'abbonamento', function(done) {
        var contatto = {contatto: 'abc.12345'};
        restAction('POST', 'AbbonamentoEstintori/' + that.abbonamento.id +'/Contatto', contatto, done, function(data, res) {
            // console.dir((data));
            expect(res.statusCode).toBe(201);
        });
    });
    it('Ricarico il cliente come oggetto composto', function(done) {
        restAction('GET', 'clienti/'+that.cliente.id, null, done, function(data, res) {
            expect(res.statusCode).toBe(200);
            that.cliente = data.data;
            that.abbonamento = that.cliente.abbonamentoEstintori[0];
        // dlog(that.cliente);
            expect(that.cliente.id).toBeTruthy();
            expect(that.cliente.numero).toBeTruthy();
            expect(that.cliente._bag.model).toBe('Cliente');
            expect(that.cliente.IndirizzoFatturaId).toBeTruthy();
            expect(that.cliente.indirizzoFattura._bag.model).toBe('Indirizzo');
            expect(that.cliente.IndirizzoId).toBeTruthy();
            expect(that.cliente.contatto[0].id).toBeTruthy();
            expect(that.cliente.contoCorrente[0].id).toBeTruthy();
            expect(that.cliente.contoCorrente[0]._bag.model).toBe('ContoCorrente');
            expect(that.abbonamento.id).toBeTruthy();
            expect(that.abbonamento.numero).toBeTruthy();
            expect(that.abbonamento.contatto[0].id).toBeTruthy();
            expect(that.abbonamento.indirizzo.id).toBeTruthy();
        });
    });
    it(' .. cambia la nota dell\'abbonamento, e l\'indirizzo di manutenzione e salva l\'oggetto composto "cliente"', function(done) {
        that.timeStamp = (new Date()).toString();
        that.cliente.ragioneSociale1 = that.timeStamp;
        that.cliente.abbonamentoEstintori[0].note = that.timeStamp;
        that.cliente.abbonamentoEstintori[0].indirizzo.localita = that.timeStamp;
        restAction('PUT', 'Cliente', that.cliente, done, function(data, res) {
            expect(res.statusCode).toBe(200);
        });
    });
    it(' .. rilegge il cliente e verifica: esistenza indirizzo fattura, nota abbonamento e localita indirizzo abbonamento', function(done) {
        restAction('GET', 'clienti/' + that.cliente.id, null, done, function(data, res) {
            var cliente = data.data;
            that.cliente = cliente;
            that.abbonamento = cliente.abbonamentoEstintori[0];
            // dlog(that.cliente);
            // dlog(cliente);
            expect(res.statusCode).toBe(200);
            expect(cliente.ragioneSociale1).toEqual(that.timeStamp);
            expect(cliente.abbonamentoEstintori[0].note).toEqual(that.timeStamp);
            expect(cliente.abbonamentoEstintori[0].indirizzo.localita).toEqual(that.timeStamp);
            expect(cliente.IndirizzoFatturaId).not.toBe(null);
        });
    });
    it('Cancella l\'indirizzo manutenzione dell\'abbonamento', function(done) {
        restAction('DELETE', 'AbbonamentoEstintori/' + that.cliente.abbonamentoEstintori[0].id
            + '/Indirizzo/' + that.cliente.abbonamentoEstintori[0].IndirizzoId,
            null, done, function(data, res) {
            expect(res.statusCode).toBe(204);
        });
    });
    it('Rilegge il secondo cliente e verifica la cancellazione dell\'indirizzo', function(done) {
        restAction('GET', 'clienti/'+that.cliente.id, null, done, function(data, res) {
            expect(res.statusCode).toBe(200);
            that.cliente = data.data;
            expect(that.cliente.abbonamentoEstintori[0].indirizzoId).toBeFalsy();
        });
    });
    it('Ottiene direttamente il contatto del cliente', function(done) {
        restAction('GET', 'Contatto/' + that.cliente.contatto[0].id, null, done, function(data, res) {
            expect(res.statusCode).toBe(200);
            expect(data.data.contatto).toBe('abc.12345');
        });
    });
    it('  .. ne richiede il cambio di descrizione', function(done) {
        var contatto = {contatto: 'name@abc.123', descrizione: 'descrizione'};
        restAction('PUT', 'Contatto/' + that.cliente.contatto[0].id, contatto, done, function(data, res) {
            expect(res.statusCode).toBe(200);
        });
    });
    it('   .. e la verifica', function(done) {
        restAction('GET', 'Contatto/' + that.cliente.contatto[0].id, null, done, function(data, res) {
            expect(res.statusCode).toBe(200);
            var contatto = data.data;
            expect(contatto.descrizione).toBe('descrizione');
            expect(contatto.contatto).toBe('name@abc.123');
        });
    });
    it('Cancello il primo cliente', function(done) {
        restAction('DELETE', 'Cliente/' + that.primoCliente.id, null, done, function(data, res) {
            expect(res.statusCode).toBe(204);
        });
    });

/*    it('Cancella l\'indirizzo del cliente', function(done) {
        restAction('DELETE', 'Cliente/' + that.cliente.id
            + '/Indirizzo/' + that.cliente.IndirizzoId,
            null, done, function(data, res) {

            that.cliente.IndirizzoId = null;
            expect(res.statusCode).toBe(204);
        });
    });

*/    it(' Cancella l\'indirizzo per invio fattura', function(done) {
        restAction('DELETE', 'Cliente/' + that.cliente.id
            + '/IndirizzoFattura/' + that.cliente.IndirizzoFatturaId,
            null, done, function(data, res) {

            that.cliente.IndirizzoFatturaId = null;
            expect(res.statusCode).toBe(204);
        });
    });
    it(' .. cancella il contatto del cliente', function(done) {
        restAction('DELETE', 'Cliente/' + that.cliente.id
            + '/Contatto/' + that.cliente.contatto[0].id,
            null, done, function(data, res) {
            expect(res.statusCode).toBe(204);
        });
    });
/*
    it(' .. cancella il conto corrente del cliente', function(done) {
        restAction('DELETE', 'Cliente/' + that.cliente.id
            + '/ContoCorrente/' + that.cliente.contoCorrente[0].id,

            null, done, function(data, res) {
            expect(res.statusCode).toBe(204);
        });
    });
    it('   .. cancello contatto abbonamento', function(done) {
        restAction('DELETE', 'AbbonamentoEstintori/'+that.abbonamento.id
            +'/Contatto/'+that.abbonamento.contatto[0].id
            , null, done, function(data, res) {
            expect(res.statusCode).toBe(204);
        });
    });
*/
    xit('Cancello il secondo cliente composto \nsenza cancellare l\'abbonamento: deve fallire', function(done) {
        restAction('DELETE', 'clienti/' + that.cliente.id, null, done, function(data, res) {
            expect(res.statusCode).toBe(500);
        });
    });
    xit('Cancello l\'abbonamento', function(done) {
        restAction('DELETE', 'Cliente/'+that.cliente.id
            +'/abbonamenti/'+that.abbonamento.id, null, done, function(data, res) {
            expect(res.statusCode).toBe(204);
        });
    });
    xit('Cancello l\'abbonamento', function(done) {
        restAction('DELETE', 'abbonamenti/'+that.abbonamento.id, null, done, function(data, res) {
            expect(res.statusCode).toBe(204);
        });
    });
    it('Cancello il secondo cliente composto', function(done) {
        // console.dir(that.cliente);
        restAction('DELETE', 'clienti/' + that.cliente.id, null, done, function(data, res) {
            console.log(util.inspect(data, inspectOpts));
            expect(res.statusCode).toBe(204);
        });
    });
    it('Quindi tutte le tabelle devono essere vuote', function(done) {
        testEmpty('AbbonamentoEstintori', done);
        testEmpty('AbbonamentoEstintoriContatto', done);
        testEmpty('Indirizzo', done);
        testEmpty('Cliente', done);
        testEmpty('ClienteContatto', done);
        testEmpty('Contatto', done);
        testEmpty('ContoCorrente', done);
        testEmpty('ClienteContatto', done);
    });
});
