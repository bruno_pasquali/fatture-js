describe('http: la richiesta a risorsa clienti', function() {

    var path = require('path');
    var fs = require('fs');
    var util = require('util');
    var utilOptions = {
        showHidden: false,
        depth: 3,
        colors: true
    };
    var log = function(data) {
        // fs.appendFileSync('logs/routeSpec.log', util.inspect(data, utilOptions) + '\n');
        fs.appendFileSync('logs/routeSpec.log', data + '\n');
        // console.log(util.inspect(data, utilOptions));
    };

    var http = require('http');
    var options = {
        hostname: 'localhost',
        port: 3000,
    };
    var limit = 10;
    var id = 0;
    var telefono = {};
    var n = 0;

    var restAction = function(action, path, item, done, expectation) {
        options.path = '/r/' + path;
        if (action === 'LIST') {
            options.path += '?limit=' + limit;
            // options.path += '?jq={"limit":' + limit + '}';
            action = 'GET';
        }
        options.method = action;
        var data = '';
        var req = http.request(options, function(res) {
            log('ACTION: ' + action);
            log('STATUS: ' + res.statusCode);
            // expect(res.statusCode).toBe(200);
            log('HEADERS: ' + JSON.stringify(res.headers));
            res.setEncoding('utf8');
            res.on('data', function(chunk) {
                data += chunk;
            });
            res.on('end', function() {
                log('BODY: ' + data);
                if ((!res.headers['content-type']) || (res.headers['content-type'].indexOf('application/json') === -1)) {
                    expectation(data, res);
                } else {
                    expectation(JSON.parse(data), res);
                }
                done();
            });
            res.on('timeout', function(chunk) {
                expect(true).toFailWithMessage('Connection timeout');
                done();
            });
        });
        req.on('error', function(e) {
            var message = 'problem with request: ' + e.message;
            console.log(message);
            expect(true).toFailWithMessage(message);
            done();
        });
        if (action === 'PUT') {
            log('sending PUT data: ' + JSON.stringify(item));
            req.setHeader("Content-Type", "application/json; charset=utf-8");
            req.write(JSON.stringify(item));
        }
        req.end();
    };

    beforeEach(function() {
        this.cliente = {};
        this.addMatchers({
            toFailWithMessage: function(expected) {
                this.message = function() {
                    return expected;
                };
                return false;
            }
        });
    });

    it('Ottiene il cliente 100 senza indirizzo fattura né banca', function(done) {
        restAction('GET', 'clienti/100', null, done, function(data, res) {
            var cliente = data.data;
            //console.dir(data);
            expect(res.statusCode).toBe(200);
            if (res.statusCode === 200) {
                expect(cliente.IndirizzoFatturaId).toBe(null);
                expect(cliente.contoCorrente.length).toBe(0);
            }
        });
    });
    it('Ottiene il cliente 200 senza indirizzo fattura, ma con banca', function(done) {
        restAction('GET', 'clienti/200', null, done, function(data, res) {
            var cliente = data.data;
            expect(res.statusCode).toBe(200);
            expect(cliente.IndirizzoFatturaId).toBe(null);
            expect(cliente.abbonamentoEstintori[0].bancaId).not.toBe(null);
        });
    });

    it('Aggiunge un indirizzo all\'abbonamento 60 del cliente 60', function(done) {
        restAction('POST', 'AbbonamentoEstintori/60/Indirizzo', null, done, function(data, res) {
            expect(res.statusCode).toBe(201);
        });
    });
    it(' .. ottiene il cliente 60', function(done) {
        restAction('GET', 'clienti/60', null, done, function(data, res) {
            this.cliente = data.data;
            expect(res.statusCode).toBe(200);
        });
    });
    it(' .. cambia la nota dell\'abbonamento, e l\'indirizzo di manutenzione e salva l\'oggetto composto "cliente"', function(done) {
        this.timeStamp = (new Date()).toString();
        this.cliente.abbonamentoEstintori[0].nota = this.timeStamp;
        this.cliente.abbonamentoEstintori[0].indirizzo.localita = this.timeStamp;
        restAction('PUT', 'cliente', this.cliente, done, function(data, res) {
            expect(res.statusCode).toBe(200);
        });
    });
    it(' .. rilegge il cliente 60 e verifica: esistenza indirizzo fattura, nota abbonamento e localita indirizzo abbonamento', function(done) {
        restAction('GET', 'clienti/60', null, done, function(data, res) {
            var cliente = data.data;
            expect(res.statusCode).toBe(200);
            expect(cliente.abbonamentoEstintori[0].nota).toEqual(this.timeStamp);
            expect(cliente.abbonamentoEstintori[0].indirizzo.localita).toEqual(this.timeStamp);
            expect(cliente.IndirizzoFatturaId).not.toBe(null);
        });
    });
    it('Cancella l\'indirizzo manutenzione dell\'abbonamento 60 del cliente 60', function(done) {
        restAction('DELETE', 'AbbonamentoEstintori/60/Indirizzo/' 
            + this.cliente.abbonamentoEstintori[0].indirizzoId, null, done, function(data, res) {
            expect(res.statusCode).toBe(204);
        });
    });

    it('Elenco clienti', function(done) {
        restAction('LIST', 'clienti', null, done, function(data, res) {
            expect(res.statusCode).toBe(200);
            var clienti = data.data;
            expect((data.data.length)).toBe(limit);
        });
    });

    it('Elenco Cliente', function(done) {
        restAction('LIST', 'Cliente', null, done, function(data, res) {
            expect(res.statusCode).toBe(200);
            var clienti = data.data;
            //console.log(data);
            expect((data.data.length)).toBe(limit);
        });
    });

    it('Ottiene il Telefono 11', function(done) {
        restAction('GET', 'Contatto/11', null, done, function(data, res) {
            expect(res.statusCode).toBe(200);
            telefono = data.data;
            expect(telefono.contatto).toBe('030/77.59.663');
        });
    });

    it('   .. ne richiede il cambio di descrizione', function(done) {
        n = parseInt(telefono.descrizione, 10);
        n = isNaN(n) ? 1 : n++;
        telefono.descrizione = n.toString();
        restAction('PUT', 'Contatto/11', telefono, done, function(data, res) {
            expect(res.statusCode).toBe(200);
        });
    });

    it('   .. e la verifica', function(done) {
        restAction('GET', 'Contatto/11', null, done, function(data, res) {
            expect(res.statusCode).toBe(200);
            telefono = {};
            telefono = data.data;
            expect(n).toBeGreaterThan(0);
            expect(telefono['descrizione']).toEqual(n.toString());
            expect(telefono['contatto']).toBe('030/77.59.663');
        });
    });

    var telefoni = [];
    var telefoni2 = [];
    n = 0;
    it('Ottiene i Telefoni del cliente 12', function(done) {
        restAction('GET', 'Cliente/12/Contatto', null, done, function(data, res) {
            expect(res.statusCode).toBe(200);
            telefoni = data.data;
            expect(telefoni[1].contatto).toBe('030/77.59.663');
        });
    });
    it('   .. ne richiede il cambio di descrizione', function(done) {
        n = parseInt(telefoni[1].descrizione, 10);
        n = isNaN(n) ? 1 : n++;
        telefoni[1].descrizione = n.toString();
        restAction('PUT', 'Contatto/' + telefoni[1].id, telefoni[1], done, function(data, res) {
            expect(res.statusCode).toBe(200);
        });
    });
    it('   .. e la verifica', function(done) {
        restAction('GET', 'Cliente/12/Contatto', null, done, function(data, res) {
            expect(res.statusCode).toBe(200);
            telefoni = data.data;
            expect(n).not.toBe(0);
            expect(telefoni[1]['descrizione']).toEqual(n.toString());
            expect(telefoni[1]['contatto']).toBe('030/77.59.663');
        });
    });

    it('   .. aggiunge un telefono', function(done) {
        restAction('POST', 'Cliente/12/Contatto', null, done, function(data, res) {
            telefoni.push(data.data);
            expect(res.statusCode).toBe(201);
        });
    });
    it('   .. e verifica', function(done) {
        restAction('GET', 'Cliente/12/Contatto', null, done, function(data, res) {
            expect(res.statusCode).toBe(200);
            telefoni2 = data.data;
            expect(telefoni2.length).toEqual(telefoni.length);
            expect(telefoni2[telefoni2.length - 1].id)
                .toEqual(telefoni[telefoni.length - 1].id);
        });
    });
    it('   .. lo cancella', function(done) {
        restAction('DELETE', 'Cliente/12/Contatto/' + telefoni2[telefoni2.length - 1].id, null, done, function(data, res) {
            expect(res.statusCode).toBe(204);
        });
    });
    it('   .. e verifica', function(done) {
        restAction('GET', 'Cliente/12/Contatto', null, done, function(data, res) {
            expect(res.statusCode).toBe(200);
            telefoni2 = data.data;
            expect(telefoni2.length).toEqual(telefoni.length - 1);
            expect(telefoni2[1].id).toEqual(telefoni[1].id);
        });
    });
    it('   .. verifica bis', function(done) {
        restAction('GET', 'Contatto/' + telefoni[telefoni.length - 1].id, null, done, function(data, res) {
            expect(res.statusCode).toBe(404);
        });
    });

    var indirizzo = {};
    it('   ne definisce l\'indirizzo', function(done) {
        restAction('POST', 'Cliente/12/Indirizzo', null, done, function(data, res) {
            expect(res.statusCode).toBe(201);
        });
    });
    it('   .. lo verifica', function(done) {
        restAction('GET', 'Cliente/12/Indirizzo', null, done, function(data, res) {
            expect(res.statusCode).toBe(200);
            indirizzo = data.data;
        });
    });
    it('   .. lo cancella', function(done) {
        restAction('DELETE', 'Cliente/12/Indirizzo/' + indirizzo.id, null, done, function(data, res) {
            expect(res.statusCode).toBe(204);
        });
    });
    it('   .. e verifica', function(done) {
        restAction('GET', 'Cliente/12/Indirizzo', null, done, function(data, res) {
            expect(res.statusCode).toBe(404);
        });
    });
    it('   .. verifica bis', function(done) {
        restAction('GET', 'Indirizzo/' + indirizzo.id, null, done, function(data, res) {
            expect(res.statusCode).toBe(404);
        });
    });


    it('Crea un Cliente ..', function(done) {
        restAction('POST', 'Cliente', null, done, function(data, res) {
            expect(res.statusCode).toBe(201);
            //console.log(data);
            id = data.data.id;
            expect(data.data['id']).not.toEqual(0);
        });
    });

    it('   .. e lo cancella', function(done) {
        restAction('DELETE', 'Cliente/' + id, null, done, function(data, res) {
            expect(res.statusCode).toBe(204);
        });
    });

    it('Crea un AbbonamentoEstintori ..', function(done) {
        restAction('POST', 'AbbonamentoEstintori', null, done, function(data, res) {
            expect(res.statusCode).toBe(201);
            id = data.data.id;
            expect(data.data['id']).not.toEqual(0);
        });
    });

    it('   .. e lo cancella', function(done) {
        restAction('DELETE', 'AbbonamentoEstintori/' + id, null, done, function(data, res) {
            expect(res.statusCode).toBe(204);
        });
    });

    it('Crea un Telefono ..', function(done) {
        restAction('POST', 'Contatto', null, done, function(data, res) {
            expect(res.statusCode).toBe(201);
            id = data.data.id;
            expect(data.data['id']).not.toEqual(0);
        });
    });

    it('   .. e lo cancella', function(done) {
        restAction('DELETE', 'Contatto/' + id, null, done, function(data, res) {
            expect(res.statusCode).toBe(204);
        });
    });

});
