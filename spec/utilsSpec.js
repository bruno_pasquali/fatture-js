describe('Utils', function() {

    var path = require('path');
    var fs = require('fs');
    var util = require('util');
    var utilOptions = {
        showHidden: false,
        depth: 3,
        colors: true
    };
    var log = function(data) {
        // fs.appendFileSync('logs/routeSpec.log', util.inspect(data, utilOptions) + '\n');
        fs.appendFileSync('logs/routeSpec.log', data + '\n');
        // console.log(util.inspect(data, utilOptions));
    };
    var myUtils = require(path.join(__dirname, '..', 'server', 'lib', 'myUtils')),
        _visit = myUtils.visit;

    var _calculateCalls = function(obj) {
        var ncalls = 0;
        _visit(obj,
            function(obj) { return obj.hasOwnProperty('_bag'); },
            function() { ncalls++; }
        );
        return ncalls;
    };

    beforeEach(function() {
        this.addMatchers({
            toFailWithMessage: function(expected) {
                this.message = function() {
                    return expected;
                };
                return false;
            }
        });
    });

    it('Visita oggetti', function() {
        var o;
        expect(_calculateCalls(o)).toBe(0);
        o = {};
        expect(_calculateCalls(o)).toBe(0);
        o = null;
        expect(_calculateCalls(o)).toBe(0);
        o = [];
        expect(_calculateCalls(o)).toBe(0);
        o = {
            id: 1, a:1, _bag: { model: 'model'}
        };
        expect(_calculateCalls(o)).toBe(1);
        o = {
            id: 1, a:1, _bag: { model: 'model1'}, vector: [
                {id:2, _bag: { model: 'model2'}}, {id:2, _bag: { model: 'model2'}}
            ]
        };
        expect(_calculateCalls(o)).toBe(3);
        o = [
            { id: 1, a:1, _bag: { model: 'model1'}, vector: [
                {id:2, _bag: { model: 'model2'}}, {id:2, _bag: { model: 'model2'}}
            ]},
            { id: 1, a:1, _bag: { model: 'model1'}, vector: [
                {id:2, _bag: { model: 'model2'}}, {id:2, _bag: { model: 'model2'}}
            ]}, 
            1, 
            []
        ];    
        expect(_calculateCalls(o)).toBe(6);
    });

});
