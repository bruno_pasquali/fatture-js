var util = require('util');

var _visit = function(obj, visitTest, callback) {
    if (!obj) return;
    var trace = function(){}; // console.log;

    if (util.isArray(obj)) {
        var i = 0,
            l = obj.length;
        for (; i < l; i++) {
            trace(' array');
            if (obj[i] && (util.isArray(obj[i]) || visitTest(obj[i]))) {
                _visit(obj[i], visitTest, callback);
            }
        }
        return;
    }
    
    if (obj === Object(obj)) {
        for (var p in obj) {
            // console.log('before property: ' + p);
            trace('Testing: ' + p);
            if (obj[p] && obj.hasOwnProperty(p) && (util.isArray(obj[p]) || visitTest(obj[p]))) {
                if (trace) trace('Entering: ' + p);
                _visit(obj[p], visitTest, callback);
            }
        }
        trace(' calling ');
        if (visitTest(obj)) callback(obj);
    }
};

exports.visit = _visit;
