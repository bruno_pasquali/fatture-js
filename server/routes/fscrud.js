// todo: rivedere risposte a PUT/POST/DELETE
// todo: refactoring

var path = require('path');
var util = require('util');
var fs = require('fs');
var rest = require(path.join(__dirname, 'rest'));
var myUtil = require(path.join(__dirname, '..', 'lib', 'myUtils')),
var basePath = path.join(__dirname, '..', 'db', 'fs')),
    _visit = myUtil.visit;
var exceptions = {
};

var redirect = function(verb, req, res, next) {
    return (function(table) {
        if ((table in exceptions) && (verb in exceptions[table])) {
            (function(m) {
                m[verb](req, res, next);
            })(require(path.join(__dirname, exceptions[table][verb])));
            return true;
        }
        return false;
    })(req.params.entity);
};

exports.list = function(req, res, next) {
    redirect('list', req, res, next)
    || (function(e) {
        models[table].findAll(req.query).ok(function (data){
            rest.sendData(req, res, data.map(function(item){return item.toPlainObject(); }));
        }).error(function(error) {
            rest.sendErrorResponse(req, res, 500, 'Get collection error', error);
        });
    })(req.params.table, req.app.get('models'));
//    prepareAndSend('list', 'findAll', req, res, next);
};

exports.item = function(req, res, next) {
    redirect('item', req, res, next)
    || (function(id, entity) {
	   	fs.readFile(path.join(basePath, entity, '.', id, '.json'), 
	   	function (error, data) {
			if (error) {
	            rest.sendErrorResponse(req, res, 401, 'Not found', error);
			}
            rest.sendData(req, res, data);
        }).error(function(error) {
            rest.sendErrorResponse(req, res, 500, 'Find error', error);
        });
    })(req.params.id, req.params.table, req.app.get('models'));
};

exports.create = function(req, res, next) {
    redirect('create', req, res, next)
    || (function(table, models) {
        models[table].create(req.body).ok(function(item) {
            // console.dir(item.toPlainObject());
            rest.sendNewItem(req, res, item.toPlainObject());
        }).error(function(error) {
            rest.sendErrorResponse(req, res, 500, 'Create error', error);
        });
    })(req.params.table, req.app.get('models'));
};

exports.update = function(req, res, next) {
    redirect('update', req, res, next)
    || (function(id, table, models) {
        models[table].find(id).ok(function(item) {
            rest.sendDataNotFound(req, res, item)
            || item.updateAttributes(req.body).success(function() {
                item.reload().ok(function(item){
                    rest.sendData(req, res, item.toPlainObject());
                });
            }).error(function(error) {
                rest.sendErrorResponse(req, res, 500, 'Update error', error);
            });
        }).error(function(error) {
            rest.sendErrorResponse(req, res, 500, 'Find error', error);
        });
    })(req.params.id, req.params.table, req.app.get('models'));
};

exports.deepUpdate = function(req, res, next) {
    redirect('deepUpdate', req, res, next)
    || _deepUpdate(req.app.get('models'), req.body, req.app.get('utils')._, function(errors) {
        if (errors.length) {
            rest.sendErrorResponse(req, res, 500, 'Errors list: ', errors);
        } else {
            rest.sendData(req, res, req.body);
        }
    });
};

var _pushError = function(cond, message, errors, done){
    if (!cond) return false;
    errors.push(message);
    done();
    return true;
};
var _update = function(models, obj, errors, done){
    if (!obj.hasOwnProperty('id')) { return done(); }  //for cross tables
    var path = ' (' + obj._bag.model + '/' + obj.id + ')';
    _pushError(!models[obj._bag.model], 'Risorsa non trovata: ' + obj._bag.model, errors, done)
    || models[obj._bag.model].find(obj.id).ok(function(item) {
        _pushError(!item, 'Risorsa non trovata: ' + path, errors, done)
        || item.updateAttributes(obj).success(function() {
            done();
        }).error(function(error) {
            _pushError(true, 'Errore in salvataggio: ' + path, errors, done);
        });
    }).error(function(error) {
        _pushError(true, 'Errore in ricerca risorsa: ' + path, errors, done);
    });
};
var _deepUpdate = function(models, obj, _, final) {
    var ncalls = _calculateCalls(obj);
    var errors = [];

    if (!ncalls || !obj._bag) final(['Dati vuoti']);

    var done = _.after(ncalls, function() {
        final(errors);
    });
    _visit(obj,
        function(obj, objName) { return objName !== '_bag'; },
        function(obj) { return obj._bag; },
        function(obj) {
            _update(models, obj, errors, done);
        }
    );
};
var _calculateCalls = function(obj) {
    var ncalls = 0;
    _visit(obj,
        function(obj, objName) { return objName !== '_bag'; },
        function(obj) { return obj._bag; },
        function() { ncalls++; }
    );
    return ncalls;
};

exports.destroy = function(req, res, next) {
    redirect('destroy', req, res, next)
    || (function(id, table, models) {
        models[table].find(id).ok(function(item) {
            rest.sendDataNotFound(req, res, item)
            || (function () { item.destroy(); }) ();
            rest.sendEmptyResponse(req, res);
        }).error(function(error) {
            rest.sendErrorResponse(req, res, 500, 'Find for delete error', error);
        });
    })(req.params.id, req.params.table, req.app.get('models'));
};

exports.subListOrItem = function(req, res, next) {
    redirect('subList', req, res, next)
    || (function(id, table, subTable, models) {
        models[table].find(id).ok(function (parent) {
            rest.sendDataNotFound(req, res, parent)
            || rest.checkSubItemParam(req, res, parent, subTable)
            || (function() {
                parent['get'+subTable]().ok( function(data) {
                    rest.sendSubData(req, res,
                        util.isArray(data)
                        ? data.map( function(item){ return item.toPlainObject(); } )
                        : data && data['toPlainObject']
                            ? data.toPlainObject()
                            : data
                    );
                }).error(function(error) {
                    rest.sendErrorResponse(req, res, 500, 'Get children collection error', error);
                });
            })();
        }).error(function(error) {
            rest.sendErrorResponse(req, res, 500, 'Get collection parent error', error);
        });
    })(req.params.id, req.params.table, req.params.subTable, req.app.get('models'));
};

exports.subCreate = function(req, res, next) {
    redirect('subCreate', req, res, next)
    || (function(id, table, subTable, models) {
        models[table].find(id).ok(function(parent) {
            rest.sendDataNotFound(req, res, parent)
            || rest.checkSubItemParam(req, res, parent, subTable)
            || (function() {
                models[subTable].create(req.body).ok(function(item) {
                    var addMethod = parent['add'+subTable]
                        ? parent['add'+subTable]
                        : parent['set'+subTable];
                    addMethod.call(parent, item).ok(function(){
                        rest.sendNewSubItem(req, res, item.toPlainObject());
                    }).error(function(error){
                        rest.sendErrorResponse(req, res, 500, 'Association error', error);
                    });
                }).error(function(error) {
                    rest.sendErrorResponse(req, res, 500, 'Create error', error);
                });
            })();
        }).error(function(error) {
            rest.sendErrorResponse(req, res, 500, 'Get parent error', error);
        });
    })(req.params.id, req.params.table, req.params.subTable, req.app.get('models'));
};

exports.subDestroy = function(req, res, next) {
    redirect('subDestroy', req, res, next)
    || (function(id, table, subId, subTable, models) {
        models[table].find(id).ok(function(parent) {
            rest.sendDataNotFound(req, res, parent)
            || rest.checkSubItemParam(req, res, parent, subTable)
            || parent['get'+subTable]({where: {id: subId}})
            .ok(function(children) {
                children = (children === []) ? null : children;
                var child = util.isArray(children) ? children[0] : children;
                rest.sendDataNotFound(req, res, children)
                || (function() {
                    var disassocFunc = parent['remove' + subTable]
                        ? function() { return parent['remove' + subTable](child); }
                        : function() { return parent['set' + subTable](null); };
                    disassocFunc().ok(function() {
                        child.destroy().ok(function() {
                            rest.sendEmptyResponse(req, res);
                        }).error(function(error) {
                            rest.sendErrorResponse(req, res, 500, 'Child delete error', error);
                        });
                    }).error(function(error){
                        rest.sendErrorResponse(req, res, 500, 'Association delete error', error);
                    });
                })();
            }).error(function(error) {
                rest.sendErrorResponse(req, res, 500, 'Find child error', error);
            });
        }).error(function(error) {
            rest.sendErrorResponse(req, res, 500, 'Find parent error', error);
        });
    })(req.params.id, req.params.table, req.params.subId, req.params.subTable, req.app.get('models'));
};
