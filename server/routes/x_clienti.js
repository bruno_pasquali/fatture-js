var path = require('path');
var rest = require(path.join(__dirname, 'rest'));

exports.list = function(req, res, next) {
	res.location(rest.fullUrl(req));
	var m = req.app.get('models');
    m.clienti.findAll(req.query).ok(function (data){
        rest.sendData(req, res, data.map(function(item){return item.toPlainObject(); }));
    }).error(function(error) {
        rest.sendErrorResponse(req, res, 500, 'Get collection error', error);
    });
};

exports.item = function(req, res, next) {
	var m = req.app.get('models');
	m.clienti.find(req.params.id).ok(function(item) {
        rest.sendDataNotFound(req, res, item)
        || rest.sendData(req, res, item.toPlainObject());
    }).error(function(error) {
        rest.sendErrorResponse(req, res, 500, 'Find error', error);
    });
};

