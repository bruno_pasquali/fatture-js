var path = require('path');
var maxNext = require(path.join(__dirname, 'maxNext'));

var _action = function(req, res, next, verb) {
    req.query = { where: { anno: req.body['anno'] } };
    maxNext[verb](req, res, next, verb, 'numero');
};

exports.create = function(req, res, next) {
    _action(req, res, next, 'create');
};
exports.subCreate = function(req, res, next) {
    _action(req, res, next, 'subCreate');
};
