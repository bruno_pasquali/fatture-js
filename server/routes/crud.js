// todo: rivedere risposte a PUT/POST/DELETE
// todo: refactoring

var path = require('path');
var util = require('util');
var fs = require('fs');
var rest = require(path.join(__dirname, 'rest'));
var myUtil = require(path.join(__dirname, '..', 'lib', 'myUtils')),
    _visit = myUtil.visit;


exports.vacuum = function(req, res, next) {
    (function(m) {
        (new m.utils.QueryChainer())
            .add(m.Contatto, 'destroy')
            .add(m.ContoCorrente, 'destroy')
            .add(m.AbbonamentoEstintori, 'destroy')
            .add(m.Cliente, 'destroy')
            .add(m.Indirizzo, 'destroy')
            .runSerially()
            .ok(function (){
                m.sequelize.query('VACUUM;')
                .ok(function (){
                    rest.sendEmptyResponse(req, res);
                }).error(function(error) {
                    rest.sendErrorResponse(req, res, 500, 'Vacuum error', error);
                });
            }).error(function(error) {
                rest.sendErrorResponse(req, res, 500, 'Vacuum error', error);
            });
    })(req.app.get('models'));
};

exports.tables = function(req, res, next) {
    var v = [];
    var ms = req.app.get('models');
    for (m in ms) {
        // if (!ms[m].name) {
        //         console.log(util.inspect(ms[m], { depth: 0 }));
        // }
        v.push(ms[m].name);
    };
    rest.sendData(
        req,
        res,
        v
    );
};

exports.schema = function(req, res, next) {
    rest.sendData(
        req,
        res,
        req.app.get('models')[req.params.table].build().toPlainObject()
    );
};

exports.list = function(req, res, next) {
    // redirect('list', req, res, next)    ||
    (function(table, models) {
        models[table].findAll(req.query).ok(function (data){
            rest.sendData(req, res, data.map(function(item){return item.toPlainObject(); }));
        }).error(function(error) {
            rest.sendErrorResponse(req, res, 500, 'Get collection error', error);
        });
    })(req.params.table, req.app.get('models'));
//    prepareAndSend('list', 'findAll', req, res, next);
};

exports.item = function(req, res, next) {
    // redirect('item', req, res, next) ||
    (function(id, table, models) {
        models[table].find(id).ok(function (item) {
            rest.sendDataNotFound(req, res, item)
            || rest.sendData(req, res, item.toPlainObject());
        }).error(function(error) {
            rest.sendErrorResponse(req, res, 500, 'Find error', error);
        });
    })(req.params.id, req.params.table, req.app.get('models'));
};

exports.create = function(req, res, next) {
    (function(table, models) {
        models[table].create(req.body).ok(function(item) {
            console.dir(req.rawBody);
            console.dir(req.headers);
            console.dir(req.body);
            rest.sendNewItem(req, res, item.toPlainObject());
        }).error(function(error) {
            rest.sendErrorResponse(req, res, 500, 'Create error', error);
        });
    })(req.params.table, req.app.get('models'));
};

exports.update = function(req, res, next) {
    (function(id, table, models) {
            console.dir(req.rawBody);
            console.dir(req.headers);
            console.dir(req.body);
        models[table].update(req.body, {id: id}).ok(function(affectedRows) {
            if (!affectedRows) { rest.sendErrorResponse(req, res, 500, 'Find error', error); }
            else { rest.sendData(req, res, req.body); }
        }).error(function(error) {
            rest.sendErrorResponse(req, res, 500, 'Update error', error);
        });
    })(req.params.id, req.params.table, req.app.get('models'));
};

exports.deepUpdate = function(req, res, next) {
            console.dir(req.rawBody);
            console.dir(req.headers);
            console.dir(req.body);
    _chain(req.app.get('models'), req.body, _update)
    .runSerially(/*{ skipOnError: true }*/)
    .ok(function() { rest.sendData(req, res, req.body); })
    .error(function(err) { rest.sendErrorResponse(req, res, 500, 'Error: ', err) });
};

var _update = function(chainer, models, obj) {
        chainer.add(models[obj._bag.model], 'update', [obj, {id: obj.id}]);
}
var _chain = function(models, obj, action) {
    var chainer = new models.utils.QueryChainer();
    _visit(obj,
        function(childObj) {
            return childObj.hasOwnProperty('_bag');
        },
        function(obj) {
            if (obj._bag.model && obj.id) {
                // console.log('Adding: ' + obj._bag.model)
                action(chainer, models, obj);
            }
        }
    );
    return chainer;
};

// var _removeAssociations = function(chainer, models, obj) {
//         chainer.add(models[obj._bag.model], 'update', [obj, {id: obj.id}]);
// }
// exports.deepDestroy = function(req, res, next) {
//     _serializeDestroy(req.app.get('models'), req.body, 'destroy')
//     .ok(function() { rest.sendEmptyResponse(req, res) })
//     .error(function(err) { rest.sendErrorResponse(req, res, 500, 'Error: ', err) });
// };
// var _serializeDeassoc = function(models, obj, verb) {
//     var chainer = new models.utils.QueryChainer();
//     _visit(obj,
//         function(childObj) {
//             return childObj.hasOwnProperty('_bag');
//         },
//         function(obj, parent) {
//             if (obj._bag.model && obj.id) {
//                 console.log('Adding: ' + obj._bag.model)
//                 chainer.add(models[obj._bag.model], verb, [{id: obj.id}]);
//             }
//         }
//     );
//     return chainer.runSerially(/*{ skipOnError: true }*/);
// };

exports.destroy = function(req, res, next) {
    (function(id, table, models) {
        models[table].destroy({id: id}).ok(function() {
            rest.sendEmptyResponse(req, res);
        }).error(function(error) {
            rest.sendErrorResponse(req, res, 500, 'Delete error', error);
        })
    })(req.params.id, req.params.table, req.app.get('models'));
};

exports.subListOrItem = function(req, res, next) {
    // redirect('subList', req, res, next) ||
    (function(id, table, subTable, models) {
        models[table].find(id).ok(function (parent) {
            rest.sendDataNotFound(req, res, parent)
            || rest.checkSubItemParam(req, res, parent, subTable)
            || (function() {
                parent['get'+subTable]().ok( function(data) {
                    rest.sendSubData(req, res,
                        util.isArray(data)
                        ? data.map( function(item){ return item.toPlainObject(); } )
                        : data && data['toPlainObject']
                            ? data.toPlainObject()
                            : data
                    );
                }).error(function(error) {
                    rest.sendErrorResponse(req, res, 500, 'Get children collection error', error);
                });
            })();
        }).error(function(error) {
            rest.sendErrorResponse(req, res, 500, 'Get collection parent error', error);
        });
    })(req.params.id, req.params.table, req.params.subTable, req.app.get('models'));
};

exports.subCreate = function(req, res, next) {
    // redirect('subCreate', req, res, next) ||
    (function(id, table, subTable, subAlias, models) {
        var alias = subAlias ? subAlias : subTable;
        models[table].find(id).ok(function(parent) {
            rest.sendDataNotFound(req, res, parent)
            || rest.checkSubItemParam(req, res, parent, alias)
            || (function() {
                models[subTable].create(req.body).ok(function(item) {
             // console.dir(req.body);
                    var addMethod = parent['add'+alias]
                        ? parent['add'+alias]
                        : parent['set'+alias];
                    addMethod.call(parent, item).ok(function(){
                        rest.sendNewSubItem(req, res, item.toPlainObject());
                    }).error(function(error){
                        rest.sendErrorResponse(req, res, 500, 'Association error', error);
                    });
                }).error(function(error) {
                    rest.sendErrorResponse(req, res, 500, 'Create error', error);
                });
            })();
        }).error(function(error) {
            rest.sendErrorResponse(req, res, 500, 'Get parent error', error);
        });
    })(req.params.id, req.params.table, req.params.subTable, req.params.subAlias, req.app.get('models'));
};

exports.subDestroy = function(req, res, next) {
    // redirect('subDestroy', req, res, next) ||
    (function(id, table, subId, subTable, models) {
        models[table].find(id).ok(function(parent) {
            rest.sendDataNotFound(req, res, parent)
            || rest.checkSubItemParam(req, res, parent, subTable)
            || parent['get'+subTable]({where: {id: subId}})
            .ok(function(children) {
                children = (children === []) ? null : children;
                var child = util.isArray(children) ? children[0] : children;
                rest.sendDataNotFound(req, res, children)
                || (function() {
                    var disassocFunc = parent['remove' + subTable]
                        ? function() { return parent['remove' + subTable](child); }
                        : function() { return parent['set' + subTable](null); };
                    disassocFunc().ok(function() {
                        child.destroy().ok(function() {
                            rest.sendEmptyResponse(req, res);
                        }).error(function(error) {
                            rest.sendErrorResponse(req, res, 500, 'Child delete error', error);
                        });
                    }).error(function(error){
                        rest.sendErrorResponse(req, res, 500, 'Association delete error', error);
                    });
                })();
            }).error(function(error) {
                rest.sendErrorResponse(req, res, 500, 'Find child error', error);
            });
        }).error(function(error) {
            rest.sendErrorResponse(req, res, 500, 'Find parent error', error);
        });
    })(req.params.id, req.params.table, req.params.subId, req.params.subTable, req.app.get('models'));
};
