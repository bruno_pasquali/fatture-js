var path = require('path');
var rest = require(path.join(__dirname, 'rest'));
var crud = require(path.join(__dirname, 'crud'));

var _action = function(req, res, next, verb, fieldName) {
    (function(table, tables) {
        var q = req.query
            ? tables[table].max(fieldName, req.query)
            : tables[table].max(fieldName);
        q.ok(function(max) {
            req.body[fieldName] = (max+1);
            crud[verb](req, res, next);
        }).error(function(error){
            rest.sendErrorResponse(req, res, 500, 'Max function error', error);
        });
    })(req.params.subTable || req.params.table, req.app.get('models'));
};

exports.create = function(req, res, next) {
    _action(req, res, next, 'create', 'numero');
};
exports.subCreate = function(req, res, next) {
    _action(req, res, next, 'subCreate', 'numero');
};
