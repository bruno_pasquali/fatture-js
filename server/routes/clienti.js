var path = require('path');
var util = require('util');
var fs = require('fs');
var rest = require(path.join(__dirname, 'rest'));
var utilOptions = {
	showHidden : false,
	depth : 3,
	colors : true
};

exports.list = function(req, res, next) {
	res.location(rest.fullUrl(req));
	var m = req.app.get('models');
	//TODO: on error
	m.clienti.findAll(m, req.query, req.app.get('utils')._, function(result) {
	    rest.sendData(req, res, result);
	});
};

exports.item = function(req, res, next) {
	var m = req.app.get('models');
	//TODO: on error
	m.clienti.find(m, req.params.id, req.app.get('utils')._, function(result) {
		// fs.appendFileSync('logs/clienti.json', JSON.stringify(result));
		// console.log(util.inspect(result, utilOptions));
    rest.sendData(req, res, result);
	// next();
		// res.location(rest.fullUrl(req));
		// res.send(result);
	});
};

