// var path = require('path');
// var rest = require(path.join(__dirname, 'rest'));
// var crud = require(path.join(__dirname, 'crud'));

// var _action = function(req, res, next, verb) {
//     (function(table, tables) {
//         tables[table].max('numero')
//         .ok(function(max) {
//             req.body['numero'] = (max+1);
//             crud[verb](req, res, next);
//         }).error(function(error){
//             rest.sendErrorResponse(req, res, 500, 'Max function error', error);
//         });
//     })(req.params.subTable || req.params.table, req.app.get('models'));
// };
// exports.create = function(req, res, next) {
//     _action(req, res, next, 'create');
// };
// exports.subCreate = function(req, res, next) {
//     _action(req, res, next, 'subCreate');
// };

var path = require('path');
var maxNext = require(path.join(__dirname, 'maxNext'));

var _action = function(req, res, next, verb) {
    req.query = null;
    maxNext[verb](req, res, next, verb, 'numero');
};

exports.create = function(req, res, next) {
    _action(req, res, next, 'create');
};
exports.subCreate = function(req, res, next) {
    _action(req, res, next, 'subCreate');
};
