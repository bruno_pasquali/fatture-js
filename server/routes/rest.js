//Ref: http://www.restapitutorial.com/resources.html

var _sendErrorResponse = function(req, res, code, errorMessage, data) {
  res.location(_fullUrl(req));
  res.send(code,
    {
      code: code,
      status: (
        code <= 399 ? 'success ???'
        : code <= 499 ? 'fail'
        : code < 599 ? 'error'
        : 'strange'
      ),
      data: data || '',
      message: errorMessage || '',
      links: { self: _fullUrl(req) }
    }
  );
  res.end();
};
exports.sendErrorResponse = _sendErrorResponse;

exports.parseQuery = function(req, res, next) {
  if (req.query.jq) {
    try {
      req.query = (JSON.parse(unescape(req.query.jq)) || {});
    } catch (e) {
      // console.log('eccezione in parseQuery');
      return _sendErrorResponse(req, res, 500, 'bad query', req.query['jq']);
    }
    return next();
  }
  if(req.query.limit) {
    var limit = parseInt(req.query.limit, 10) || 25;
    var offset = parseInt(req.query.offset, 10) || 0;
    var nextOffset = limit + offset;
    var prevOffset = Math.max(0, limit - offset);
    req.collectionQueryLinks = {
      limit: limit,
      offset: offset,
      next: 'limit=' + limit + '&offset=' + nextOffset,
      previous: 'limit=' + limit + '&offset=' + prevOffset
    };
  }
  return next();
};

var _setRange = function (req, res) {
    if(req.collectionQueryLinks) {
      var l = req.collectionQueryLinks;
      res.set('Content-Range', 'items ' + l.offset + '-' + (l.offset + l.limit-1) + '/*');
    }
};
var _enrichLinks = function (req, obj) {
    if(req.collectionQueryLinks) {
      var l = req.collectionQueryLinks;
      var url = _fullUrl(req).split('?')[0];
      obj.links.next = url + '?' + l.next;
      obj.links.previous = url + '?' + l.previous;
    }
    return obj;
};

exports.checkSubItemParam = function(req, res, item, subItemName) {
  if (item['get' + subItemName]) { return false; }
  _sendErrorResponse(req, res, 404, '3: Wrong resource child name: ' + subItemName);
  return true;
};
exports.checkTableParam = function(req, res, table) {
  if (req.app.get('models')[table]) { return false; }
  _sendErrorResponse(req, res, 404, '1: Wrong resource name: ' + table);
  return true;
};
var _sendDataNotFound = function(req, res, data) {
  if (data) { return false; }
  _sendErrorResponse(req, res, 404, '2: Resource not found: ' + req.path);
  return true;
};
exports.sendDataNotFound = _sendDataNotFound;

var _fullUrl = function(req) {
  return req.protocol + '://' + req.get('host') + req.url;
};
exports.fullUrl = _fullUrl;

exports.sendEmptyResponse = function(req, res) {
  res.location(_fullUrl(req));
  res.send(204);
};
exports.sendNewItem = function(req, res, data) {
  var url = _fullUrl(req) + (data['id'] ? '/' + data.id.toString() : '');
  res.location(url);
  res.send(201,
    {
      code: 201,
      status: 'success',
      data: data || {},
      links: {
        self: url,
        self_name: req.params.subTable
      }
    }
  );
};
exports.sendNewSubItem = function(req, res, data) {
  var url = _fullUrl(req) + (data['id'] ? '/' + data.id.toString() : '');
  res.location(url);
  res.send(201,
    {
      code: 201,
      status: 'success',
      data: data || {},
      links: {
        self: url,
        self_name: req.params.subTable,
        parent_name: req.params.table,
        parent_id: req.params.id
      }
    }
  );
};

exports.sendData = function(req, res, data) {
  res.location(_fullUrl(req));
  _setRange(req, res);
  _sendDataNotFound(req, res, data)
  || res.send(200, _enrichLinks(req, {
      code: 200,
      status: 'success',
      data: data || {},
      links: {
        self: _fullUrl(req),
        self_name: req.params.table
      }
  }));
};

exports.sendSubData = function(req, res, data) {
  res.location(_fullUrl(req));
  _setRange(req, res);
  _sendDataNotFound(req, res, data)
  || res.send(200, _enrichLinks(req, {
    code: 200,
    status: 'success',
    data: data || {},
    links: {
      self: _fullUrl(req),
      self_name: req.params.subTable,
      parent_name: req.params.table,
      parent_id: req.params.id
    }
  }));
};
