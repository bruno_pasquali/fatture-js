var path = require('path'),
    defaultConfig = 'production';

if (process.env.OPENSHIFT_NODEJS_PORT) {
    process.env.NODE_ENV = 'openShift';
}

var configSet = require(path.join(__dirname, 'configSet'));

if (configSet.hasOwnProperty(process.env.NODE_ENV)) {
    module.exports = configSet[process.env.NODE_ENV];
} else {
    console.log(' ***** Config not found, loading: ' + defaultConfig);
    module.exports = configSet[defaultConfig];
}
