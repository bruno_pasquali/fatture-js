module.exports = {
    development: {
        PORT: 3001,
        IP: "0.0.0.0",
        env: 'development',
        db: {
            database: 'db',
            username: 'app',
            password: 'app',
            options: {
                storage: 'db/dev.sqlite3',
                dialect: 'sqlite'
            }
        }
    },
    production: {
        PORT: 3000,
        IP: "127.0.0.1",
        env: 'production',
        db: {
            database: 'db',
            username: 'app',
            password: 'app',
            options: {
                storage: 'db/db.sqlite3',
                dialect: 'sqlite'
            }
        }
    },
    test: {
        PORT: 3002,
        IP: "127.0.0.1",
        env: 'test',
        db: {
            database: 'test.sqlite3',
            username: 'app',
            password: 'app',
            options: {
                storage: 'db/test.sqlite3',
                dialect: 'sqlite'
            }
        }
    },
    openShift: {
        PORT: process.env.OPENSHIFT_NODEJS_PORT || "8080",
        IP: process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1",
        env: 'production',
        db: {
            database: 'db',
            username: 'app',
            password: 'app',
            options: {
                // host: 'localhost',
                // port: '5432',
                storage: 'db.sqlite3',
                dialect: 'sqlite'
            }
        }
    }
};
