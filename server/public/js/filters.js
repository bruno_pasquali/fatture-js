'use strict';

/* Filters */

angular.module('myApp.filters', [])
.filter('interpolate', ['version', function(version) {
    return function(text) {
        return String(text).replace(/\%VERSION\%/mg, version);
    }
}])
/*
.filter('split', function() {
    return function(input, splitChar) {
        if (!input) { return; }
        return input.split(splitChar);
    }
})*/
;
