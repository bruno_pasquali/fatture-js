'use strict';

/* Controllers */

angular.module('myApp.controllers', []).
controller('MainCtrl', [
    function() {

    }
]).controller('FormContattiCtrl', ['$scope', '$http',
    function FormContattiCtrl($scope, $http) {
        $scope.tipiContatto = myScope.tipiContatto;
        $scope.new = function() {
            var p = $scope.parent;
            $http.post('/r/' + p._bag.model + '/' + p.id + '/Contatto').success(function(data) {
                p.contatto.push(data.data);
            }).error(function(data, status) {
                alert('errore: ' + angular.toJson(data, true));
            });
        };
        $scope.delete = function(index) {
            if (!confirm("Confermi la cancellazione ?")) { return }
            var p = $scope.parent;
            var contatto = p.contatto[index];
            $http.delete('/r/' + p._bag.model + '/' + p.id + '/' + contatto._bag.model + '/' + contatto.id).success(function(data) {
                p.contatto.splice(index, 1);
            }).error(function(data, status) {
                alert('errore: ' + angular.toJson(data, true));
            });
        };
    }
]).controller('FormContiCorrentiCtrl', ['$scope', '$http',
    function FormContiCorrentiCtrl($scope, $http) {
        $scope.new = function() {
            var p = $scope.cliente;
            $http.post('/r/' + p._bag.model + '/' + p.id + '/ContoCorrente').success(function(data) {
                $scope.conti.push(data.data);
            }).error(function(data, status) {
                alert('errore: ' + angular.toJson(data, true));
            });
        };
        $scope.delete = function(index) {
            if (!confirm("Confermi la cancellazione ?")) { return }
            var p = $scope.cliente;
            var conto = $scope.conti[index];
            $http.delete('/r/' + p._bag.model + '/' + p.id + '/' + conto._bag.model + '/' + conto.id).success(function(data) {
                $scope.conti.splice(index, 1);
            }).error(function(data, status) {
                alert('errore: ' + angular.toJson(data, true));
            });
        };
    }
]).controller('ElencoComuniCtrl', ['$scope', '$http', 'ngTableParams',
    function ElencoComuniCtrl($scope, $http, ngTableParams) {
        $scope.comuni = [];

        $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 20000, // count per page
            sorting: {
                comune: 'asc' // initial sorting
            }
        }, {
            total: 0, // length of data
            getData: function($defer, params) {
                $http.get('/r/Comune/?limit=' + params.count()).success(function(data) {
                    $scope.comuni = data.data || [];
                    params.total($scope.comuni.length)
                    $defer.resolve($scope.comuni);
                }).error(function(data, status) {
                    alert('errore: ' + angular.toJson(data, true));
                    $defer.resolve($scope.comuni);
                });
            }
        });
        // $scope.contatti = $scope.abbonamento.telefono || [];
        $scope.new = function() {
            // $http.post('/r/' + parentModel + '/' + a.id + '/Telefono').success(function(data) {
            //     $scope.contatti.push(data.data);
            // });
        };
        $scope.delete = function(index) {
            // $http.delete('/r/' + parentModel + '/' + a.id + '/' + contatto._bag.model + '/' + contatto.id).success(function(data) {
            //     $scope.contatti.splice(index,1);
            // });
        };
    }
]);
