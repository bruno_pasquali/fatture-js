'use strict';
(function (scope) {
	var _setDefault = scope.utils.setDefault;	
	var _forceFloat = scope.utils.forceFloat;	
	var _forceInt = scope.utils.forceInt;	

	var _IBAN = function () {
		return (
			Math.round((_forceFloat(this.costoSpese) + _forceFloat(this.costo)) 
			* (_forceFloat(this.IVA) - _forceFloat(this.ritenuta) + 100.0)) / 100
			|| 0.0
		);
		
	};

	//df.fm. per default
	var _dataScadenza = function () {
		var mese = _forceInt(this.meseEmissione) + Math.round(_forceInt(this.giorniPagamento)/30);
		//js: mese 1 = Febbraio, giorno 0 = ultimo del mese precedente
		//meseEmissione: mese 2 = Febbraio
		var data = new Date((new Date()).getFullYear(), mese, 0);
		return data; //new Date((new Date()).getFullYear(), mese, 0);
	};

	scope.ContoCorrente = function(initObj) {
		_.assign(this, initObj);

		_setDefault(this, 'ABI', '00000');
		_setDefault(this, 'CAB', '00000')
		_setDefault(this, 'contoCorrente', '000000000000')

		this.IBAN = myScope.IBAN;
		this.dataScadenza = _dataScadenza.bind(this);
	} 
})(myScope);