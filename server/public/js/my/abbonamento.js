'use strict';
if (typeof module !== 'undefined' && module.exports !== 'undefined') {
    var myScope = require('./scope');
    var _ = require('lodash');

}
(function (scope) {

    var _setDefault = scope.utils.setDefault;
    var _forceInt = scope.utils.forceInt;
    var _forceFloat = scope.utils.forceFloat;

    var _totale = function () {
        var costo = _imponibile.call(this) * (parseFloat(this.IVA) - parseFloat(this.ritenuta) + 100.0) / 100;
        if (isNaN(costo)) { throw 'Errore calcolo fattura'; }
        return costo;
    };
    var _imponibile = function () {
        var costo = parseFloat(this.costoSpese) + parseFloat(this.costo);
        if (isNaN(costo)) { throw 'Errore calcolo fattura'; }
        return costo;
    };
	//df.fm. per default
	var _dataScadenza = function (mese) {
		var m = _forceInt(mese || this.meseEmissione)
            + Math.round(_forceInt(this.giorniPagamento)/30);
		//js: mese 1 = Febbraio, giorno 0 = ultimo del mese precedente
		//meseEmissione: mese 2 = Febbraio
		var data = new Date((new Date()).getFullYear(), m, 0);
		return data; //new Date((new Date()).getFullYear(), mese, 0);
	};

	scope.Abbonamento = function(initObj) {
        var _setDefault = scope.utils.setDefault;
		_.assign(this, initObj);
		_setDefault(this, 'costoSpese', 0);
		_setDefault(this, 'costo', 0)
		_setDefault(this, 'IVA', 0)
		_setDefault(this, 'ritenuta', 0)

        this.totale = _totale.bind(this);
        this.imponibile = _imponibile.bind(this);
		this.dataScadenza = _dataScadenza.bind(this);
	}

    if (typeof module !== 'undefined' && module.exports !== 'undefined') {
        module.exports = scope;
    }

})(myScope);

/*

 {
    "id": 3,
    "numero": 3,
    "ragioneSociale": "VOGLIA di PIZZA snc di Bertazzoli Gaia & C.",
    "codiceFiscale": "BRTGAI74H68I437Q",
    "partitaIVA": "02343090169",
    "note": " ",
    "createdAt": "2014-12-30T19:13:15.748Z",
    "updatedAt": "2015-01-05T20:44:02.000Z",
    "IndirizzoFatturaId": null,
    "IndirizzoId": 4,
    "contatto": [
      {
        "id": 2,
        "contatto": "030/74.02.339",
        "descrizione": "",
        "tipo": 101,
        "createdAt": "2014-12-30T19:13:15.748Z",
        "updatedAt": "2015-01-05T20:44:02.000Z",
        "clienteContatto": {
          "createdAt": "2014-12-30 20:13:15.748285",
          "updatedAt": "2014-12-30 20:13:15.748285",
          "ContattoId": 2,
          "ClienteId": 3,
          "_bag": {
            "model": "ClienteContatto",
            "readOnly": false
          }
        },
        "_bag": {
          "model": "Contatto",
          "readOnly": false
        }
      }
    ],
    "contoCorrente": [],
    "indirizzo": {
      "id": 4,
      "indirizzo": "Via Giovanni Bosco, 14 ",
      "localita": "",
      "note": "",
      "CAP": "24060",
      "createdAt": "2014-12-30T19:13:15.748Z",
      "updatedAt": "2015-01-05T20:44:02.000Z",
      "ComuneId": 7971,
      "comune": {
        "id": 7971,
        "istat": 16242,
        "comune": "Villongo",
        "CAP": "24060",
        "provincia": "BG",
        "regione": "LOM",
        "prefisso": "035",
        "codiceFisco": "M045",
        "createdAt": "2014-12-30 20:13:15.748285",
        "updatedAt": "2015-01-05 20:44:02.000 +00:00",
        "_bag": {
          "model": "Comune",
          "readOnly": false
        }
      },
      "_bag": {
        "model": "Indirizzo",
        "readOnly": false
      }
    },
    "indirizzoFattura": null,
    "abbonamentoEstintori": [
      {
        "id": 3,
        "numero": 3,
        "descrizionePagamento": "DF.FM.",
        "note": " ",
        "costo": 43,
        "numeroEstintori": 1,
        "IVA": 22,
        "descrizioneSpese": "",
        "costoSpese": 0,
        "seDisdetta": false,
        "giornoEmissione": 2,
        "meseEmissione": 1,
        "giorniPagamento": 60,
        "seFatturareAGennaio": true,
        "dataFatturaFineMese": true,
        "ritenuta": 0,
        "tipoPagamento": 202,
        "tipo": 0,
        "createdAt": "2014-12-30T19:13:15.748Z",
        "updatedAt": "2015-01-05T20:44:02.000Z",
        "ClienteId": 3,
        "IndirizzoId": 5,
        "contatto": [],
        "indirizzo": {
          "id": 5,
          "indirizzo": "Via S. Pancrazio, 27 ",
          "localita": "",
          "note": "",
          "CAP": "25036",
          "createdAt": "2014-12-30T19:13:15.748Z",
          "updatedAt": "2015-01-05T20:44:02.000Z",
          "ComuneId": 4928,
          "comune": {
            "id": 4928,
            "istat": 17133,
            "comune": "Palazzolo sull'Oglio",
            "CAP": "25036",
            "provincia": "BS",
            "regione": "LOM",
            "prefisso": "030",
            "codiceFisco": "G264",
            "createdAt": "2014-12-30 20:13:15.748285",
            "updatedAt": "2015-01-05 20:44:02.000 +00:00",
            "_bag": {
              "model": "Comune",
              "readOnly": false
            }
          },
          "_bag": {
            "model": "Indirizzo",
            "readOnly": false
          }
        },
        "_bag": {
          "model": "AbbonamentoEstintori",
          "readOnly": false
        }
      },
      {
        "id": 706,
        "numero": 655,
        "descrizionePagamento": "",
        "note": "",
        "costo": 0,
        "numeroEstintori": 0,
        "IVA": 0,
        "descrizioneSpese": "",
        "costoSpese": 0,
        "seDisdetta": false,
        "giornoEmissione": 1,
        "meseEmissione": 1,
        "giorniPagamento": 0,
        "seFatturareAGennaio": false,
        "dataFatturaFineMese": true,
        "ritenuta": 0,
        "tipoPagamento": 0,
        "tipo": 0,
        "createdAt": "2015-01-05T20:43:38.000Z",
        "updatedAt": "2015-01-05T20:44:02.000Z",
        "ClienteId": 3,
        "IndirizzoId": null,
        "contatto": [],
        "indirizzo": null,
        "_bag": {
          "model": "AbbonamentoEstintori",
          "readOnly": false
        }
      }
    ],
    "_bag": {
      "model": "Cliente",
      "readOnly": false
    }
*/
