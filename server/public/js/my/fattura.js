'use strict';
if (typeof module !== 'undefined' && module.exports !== 'undefined') {
    var myScope = {}
    var _ = require('lodash');
}
(function (scope) {

    var assignIndirizzo = function (fattura, indirizzo, postfix)  {
        fattura['indirizzo'+postfix] = indirizzo.indirizzo;
        fattura['localita'+postfix] = indirizzo.localita;
        fattura['CAP'+postfix] = indirizzo.CAP;
        fattura['comune'+postfix] = indirizzo.comune.comune;
        fattura['provincia'+postfix] = indirizzo.comune.provincia;
    };
    var _emetti = function (dataEmissione) {
        var data;
        if (!this['dataScadenza']) { return; }
        data = dataEmissione || (new Date());
        this.dataEmissione = data;
        this.dataPagamento = this.dataScadenza(data.getMonth()+1);
        this.anno = data.getFullYear();
    };

    //flatten a customer with his subscription
    scope.Fattura = function(initObj) {
        var cliente, abbonamento, conto;
        //beware the order: some plain property
        //  MUST overwrite object properties with same name
        //  see "null" assignments
        abbonamento = initObj['abbonamento'];
        cliente = initObj['cliente'];
        if (abbonamento) {
            scope.Abbonamento.call(this, abbonamento); //super()
            // _.assign(this, cliente);
            this.AbbonamentoId = abbonamento.id;
            this.indirizzo = null;
            if (abbonamento['indirizzo']) {
                assignIndirizzo(this, abbonamento.indirizzo, 'Manutenzione');
            }
        } else {
            //anyway getElementsByTagName('') methods
            scope.Abbonamento.call(this); //super()
        }
        if (cliente) {
            _.assign(this, cliente);
            this.indirizzo = null;
            this.contoCorrente = null;
            this.ClienteId = cliente.id;
            if (cliente['indirizzo']) {
                _.assign(this, cliente.indirizzo);
                this.comune = null;
                _.assign(this, cliente.indirizzo.comune);
                this.indirizzo = cliente.indirizzo.indirizzo;
            }
            if (cliente['indirizzoFattura']) {
                assignIndirizzo(this, cliente.indirizzoFattura, 'Fattura');
            }
            if (cliente['contoCorrente'] && cliente.contoCorrente.length) {
                conto = cliente.contoCorrente[0];
                _.assign(this, conto);
                this.contoCorrente = conto.contoCorrente;
            }
        }
        if (initObj['fattura']) {
            _.assign(this, initObj['fattura']);
        }

        this.emetti = _emetti.bind(this);
        this.id = null;
        this.emetti();
    }
    // scope.Fattura.prototype = new scope.Abbonamento();

})(myScope);
if (typeof module !== 'undefined' && module.exports !== 'undefined') {
    module.exports = myScope;
}



/*

INSERT INTO `StoricoFattureEstintori`
(`id`,`ClienteId`,`AbbonamentoId`,`anno`,`numero`,`numeroPrecedente`,`ragioneSociale`,`codiceFiscale`,`partitaIVA`,`indirizzo`,`localita`,`comune`,`CAP`,`provincia`,`indirizzoSpedizione`,`localitaSpedizione`,`comuneSpedizione`,`CAPSpedizione`,`provinciaSpedizione`,`indirizzoManutenzione`,`localitaManutenzione`,`comuneManutenzione`,`CAPManutenzione`,`provinciaManutenzione`,`note`,`descrizionePagamento`,`costo`,`numeroEstintori`,`IVA`,`descrizioneSpese`,`costoSpese`,`dataEmissione`,`dataPagamento`,`dataFatturaFineMese`,`seNotaAccredito`,`seDisdetta`,`giorniPagamento`,`tipoPagamento`,`ritenuta`,`ABI`,`CAB`,`banca`,`filiale`,`contoCorrente`,`IBAN`,`createdAt`,`updatedAt`)
VALUES (NULL,0,0,0,0,0,'','','','','','','','','','','','','','','','','','','','',0,0,0,'',0,0,0,1,0,0,0,0,0,'','','','','','','2015-01-21 22:53:18.000 +00:00','2015-01-21 22:53:18.000 +00:00');

"id": null,
    "ClienteId": 0,
    "AbbonamentoId": 0,
    "anno": 0,
    "numero": 0,
    "numeroPrecedente": 0,
    "ragioneSociale": "",
    "codiceFiscale": "",
    "partitaIVA": "",
    "indirizzo": "",
    "localita": "",
    "comune": "",
    "CAP": "",
    "provincia": "",
    "indirizzoSpedizione": "",
    "localitaSpedizione": "",
    "comuneSpedizione": "",
    "CAPSpedizione": "",
    "provinciaSpedizione": "",
    "indirizzoManutenzione": "",
    "localitaManutenzione": "",
    "comuneManutenzione": "",
    "CAPManutenzione": "",
    "provinciaManutenzione": "",
    "note": "",
    "descrizionePagamento": "",
    "costo": 0,
    "numeroEstintori": 0,
    "IVA": 0,
    "descrizioneSpese": "",
    "costoSpese": 0,
    "dataEmissione": 0,
    "dataPagamento": 0,
    "dataFatturaFineMese": true,
    "seNotaAccredito": false,
    "seDisdetta": false,
    "giorniPagamento": 0,
    "tipoPagamento": 0,
    "ritenuta": 0,
    "ABI": "",
    "CAB": "",
    "banca": "",
    "filiale": "",
    "contoCorrente": "",
    "IBAN": "",

{
  "code": 200,
  "status": "success",
  "data": {
    "id": 3,
    "numero": 3,
    "ragioneSociale": "VOGLIA di PIZZA snc di Bertazzoli Gaia & C.",
    "codiceFiscale": "BRTGAI74H68I437Q",
    "partitaIVA": "02343090169",
    "note": " ",
    "createdAt": "2014-12-30T19:13:15.748Z",
    "updatedAt": "2015-01-05T20:44:02.000Z",
    "IndirizzoFatturaId": null,
    "IndirizzoId": 4,
    "contatto": [
      {
        "id": 2,
        "contatto": "030/74.02.339",
        "descrizione": "",
        "tipo": 101,
        "createdAt": "2014-12-30T19:13:15.748Z",
        "updatedAt": "2015-01-05T20:44:02.000Z",
        "clienteContatto": {
          "createdAt": "2014-12-30 20:13:15.748285",
          "updatedAt": "2014-12-30 20:13:15.748285",
          "ContattoId": 2,
          "ClienteId": 3,
          "_bag": {
            "model": "ClienteContatto",
            "readOnly": false
          }
        },
        "_bag": {
          "model": "Contatto",
          "readOnly": false
        }
      }
    ],
    "contoCorrente": [],
    "indirizzo": {
      "id": 4,
      "indirizzo": "Via Giovanni Bosco, 14 ",
      "localita": "",
      "note": "",
      "CAP": "24060",
      "createdAt": "2014-12-30T19:13:15.748Z",
      "updatedAt": "2015-01-05T20:44:02.000Z",
      "ComuneId": 7971,
      "comune": {
        "id": 7971,
        "istat": 16242,
        "comune": "Villongo",
        "CAP": "24060",
        "provincia": "BG",
        "regione": "LOM",
        "prefisso": "035",
        "codiceFisco": "M045",
        "createdAt": "2014-12-30 20:13:15.748285",
        "updatedAt": "2015-01-05 20:44:02.000 +00:00",
        "_bag": {
          "model": "Comune",
          "readOnly": false
        }
      },
      "_bag": {
        "model": "Indirizzo",
        "readOnly": false
      }
    },
    "indirizzoFattura": null,
    "abbonamentoEstintori": [
      {
        "id": 3,
        "numero": 3,
        "descrizionePagamento": "DF.FM.",
        "note": " ",
        "costo": 43,
        "numeroEstintori": 1,
        "IVA": 22,
        "descrizioneSpese": "",
        "costoSpese": 0,
        "seDisdetta": false,
        "giornoEmissione": 2,
        "meseEmissione": 1,
        "giorniPagamento": 60,
        "seFatturareAGennaio": true,
        "dataFatturaFineMese": true,
        "ritenuta": 0,
        "tipoPagamento": 202,
        "tipo": 0,
        "createdAt": "2014-12-30T19:13:15.748Z",
        "updatedAt": "2015-01-05T20:44:02.000Z",
        "ClienteId": 3,
        "IndirizzoId": 5,
        "contatto": [],
        "indirizzo": {
          "id": 5,
          "indirizzo": "Via S. Pancrazio, 27 ",
          "localita": "",
          "note": "",
          "CAP": "25036",
          "createdAt": "2014-12-30T19:13:15.748Z",
          "updatedAt": "2015-01-05T20:44:02.000Z",
          "ComuneId": 4928,
          "comune": {
            "id": 4928,
            "istat": 17133,
            "comune": "Palazzolo sull'Oglio",
            "CAP": "25036",
            "provincia": "BS",
            "regione": "LOM",
            "prefisso": "030",
            "codiceFisco": "G264",
            "createdAt": "2014-12-30 20:13:15.748285",
            "updatedAt": "2015-01-05 20:44:02.000 +00:00",
            "_bag": {
              "model": "Comune",
              "readOnly": false
            }
          },
          "_bag": {
            "model": "Indirizzo",
            "readOnly": false
          }
        },
        "_bag": {
          "model": "AbbonamentoEstintori",
          "readOnly": false
        }
      },
      {
        "id": 706,
        "numero": 655,
        "descrizionePagamento": "",
        "note": "",
        "costo": 0,
        "numeroEstintori": 0,
        "IVA": 0,
        "descrizioneSpese": "",
        "costoSpese": 0,
        "seDisdetta": false,
        "giornoEmissione": 1,
        "meseEmissione": 1,
        "giorniPagamento": 0,
        "seFatturareAGennaio": false,
        "dataFatturaFineMese": true,
        "ritenuta": 0,
        "tipoPagamento": 0,
        "tipo": 0,
        "createdAt": "2015-01-05T20:43:38.000Z",
        "updatedAt": "2015-01-05T20:44:02.000Z",
        "ClienteId": 3,
        "IndirizzoId": null,
        "contatto": [],
        "indirizzo": null,
        "_bag": {
          "model": "AbbonamentoEstintori",
          "readOnly": false
        }
      }
    ],
    "_bag": {
      "model": "Cliente",
      "readOnly": false
    }
  },
  "links": {
    "self": "http://localhost:3001/r/clienti/3",
    "self_name": "clienti"
  }
}

*/
