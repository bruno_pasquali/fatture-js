var _ = require('lodash');
var scope = require(__dirname+'/scope');
_.assign(scope, require(__dirname+'/fattura'));
_.assign(scope, require(__dirname+'/abbonamento'));
module.exports = scope;
