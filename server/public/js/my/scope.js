'use strict';
if (typeof module !== 'undefined' && module.exports !== 'undefined') {
    var _ = require('lodash');
}
var myScope = (function(scope) {
    scope.utils = {};
    var utils = scope.utils;

    utils.capitalize = function capitalize(string) {
        var s = string || '';
        return s.charAt(0).toUpperCase() + s.slice(1);
    };

    utils.setDefault = function(obj, prop, defaultValue) {
        obj[prop] = obj[prop] || defaultValue;
    }
    utils.forceFloat = function(float) {
        return parseFloat(float) || 0.0;
    }
    utils.forceInt = function(int) {
        return parseInt(int, 10) || 0;
    }

    scope.NESSUNO = 0;
    scope.TELEFONO = 101;
    scope.FAX = 102;
    scope.CELLULARE = 103;
    scope.EMAIL = 104;

    scope.RIBA = 201;
    scope.RIMESSA_DIRETTA = 202;
    scope.BONIFICO = 203;

    scope.tipiContatto = [{
        tipo: scope.NESSUNO,
        nome: ''
    }, {
        tipo: scope.TELEFONO,
        nome: 'telefono'
    }, {
        tipo: scope.FAX,
        nome: 'fax'
    }, {
        tipo: scope.CELLULARE,
        nome: 'cellulare'
    }, {
        tipo: scope.EMAIL,
        nome: 'email'
    }];
    scope.tipiPagamento = [{
        tipo: scope.NESSUNO,
        nome: ''
    }, {
        tipo: scope.RIBA,
        nome: 'ri.ba.'
    }, {
        tipo: scope.RIMESSA_DIRETTA,
        nome: 'rimessa diretta'
    }, {
        tipo: scope.BONIFICO,
        nome: 'bonifico'
    }];

    return scope;
})({});
if (typeof module !== 'undefined' && module.exports !== 'undefined') {
    module.exports = myScope;
}
