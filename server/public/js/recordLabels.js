//recordLabels.js
rl = {
	cliente: {
        numero: { label: null , 
        	type: 'integer' , allowNull: false },
        ragioneSociale: { type: 'varchar(255)'  },
        codiceFiscale: { type: 'char(16)'  },
        partitaIVA: { type: 'char(12)'  },
        note: { type: 'varchar(255)' , defaultValue: '' },
        indirizzoFatturaId: { type: 'integer' , references: 'Indirizzo', referencesKey: 'id' },
        IndirizzoId: { type: 'integer' , references: 'Indirizzo', referencesKey: 'id' },
	}

};

/*
i18n = {
	numero: { it: "Numero", en: ""}
};
*/