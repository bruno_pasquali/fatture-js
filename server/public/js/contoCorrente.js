'use strict';
(function(scope) {
    var _setDefault = scope.utils.setDefault;
    // var _forceFloat = scope.utils.forceFloat;	
    // var _forceInt = scope.utils.forceInt;	

    function r0(s, l) {
        var c = "" + s;
        while (c.length < l) c = "0" + c;
        return (c);
    }

    function _calcolaBBAN() {
        if (!this.validateABI() || !this.validateCAB() || !this.validateContoCorrente()) {
        	return ("");
        }
        var vABI = r0(this.ABI, 5);
        var vCAB = r0(this.CAB, 5);
        var vNCC = r0(this.contoCorrente, 12);
        chS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        odA = new Array(1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 2, 4, 18, 20, 11, 3, 6, 8, 12, 14, 16, 10, 22, 25, 24, 23);
        evA = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25);
        var DAN = "" + vABI + vCAB + vNCC;
        var s = 0;
        var i = -1;
        while (i !== 21) {
            s += odA[chS.indexOf(DAN.charAt(++i))] + evA[chS.indexOf(DAN.charAt(++i))];
        }
        return (chS.charAt(10 + s % 26) + vABI + vCAB + vNCC);
    }

    function _calcolaIBAN() {
    	var pBBAN = _calcolaBBAN();
        // if (pBBAN.search(/^[A-Z][0-9]{5}[0-9]{5}[A-Z0-9]{12}$/) == -1) {
        if (!pBBAN) {return (""); }
        var vIBAN = pBBAN + "IT00";
        var w = "";
        for (var i = 0; i < 27; i++) {
            w += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(vIBAN.charAt(i));
        }
        while (w.length > 2) {
            w = parseInt(w.substr(0, 7), 10) % 97 + w.substr(7);
        }
        return ("IT" + r0(98 - parseInt(w, 10) % 97, 2) + pBBAN);
    }

    function _verificaIBAN() {
        if (this.search(/^IT[0-9]{2}[A-Z][0-9]{5}[0-9]{5}[A-Z0-9]{12}$/) == -1) {
            return false;
        }
        return (_calcolaIBAN() === IBAN);
    }

    var _validateABI = function() {
        return (
        	this.ABI.search(/^[0-9]{1,5}$/) !== -1 
        	? { valid: true}
        	: { valid: false, error:'Il codice ABI deve essere un numero di max 5 cifre'}
        )
    };
    var _validateCAB = function(this) {
        return (this.CAB.search(/^[0-9]{1,5}$/) !== -1)
    };
    var _validateContoCorrente = function(this) {
        return (this.contoCorrente.search(/^[0-9A-Z]{1,12}$/) !== -1)
    };
    var _validateCIN = function(this) {
        return true
    };
    var _validateIBAN = function(this) {
        return true
    };
    var _validate = function(this) {
        var validation = {
            checks: [ 
            	_validateABI(this),
            	_validateCAB(this),
            	_validateContoCorrente(this),
            	_validateCIN(this),
            	_validateIBAN(this)
            ]
        }
        validation.valid = validation.validABI && validation.validCAB && validation.validCIN && validation.validIBAN;
    };

    scope.ContoCorrente = function(initObj) {
        _.assign(this, initObj);

        _setDefault(this, 'ABI', '00000');
        _setDefault(this, 'CAB', '00000');
        _setDefault(this, 'contoCorrente', '')

        // this.calcolaIBAN = _calcolaIBAN.bind(this);
        this.verificaIBAN = _verificaIBAN.bind(this);
        // this.validate = _validate.bind(this);
    }
})(myScope);


/*

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Calcolo CIN / BBAN / IBAN</title>
</head>

<body>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//IT">
<html>
<head>


  <title>INAS-CISL Milano - Calcolo CIN BBAN IBAN</title>
  <meta name="DESCRIPTION" content="Calcolo in Javascript di CIN BBAN IBAN a partire da ABI CAB e numero conto corrente">
  <meta name="CREATED" content="20061218;08000000">
  <meta name="CHANGED" content="20061218;08000000">
  <meta name="AUTHOR" content="dvd for InasMI">

  <script type="text/javascript">
<!--
function check_scriptVersion(){
   var s;
   s = "";
   s += ScriptEngine() + " Version ";
   s += ScriptEngineMajorVersion() + ".";
   s += ScriptEngineMinorVersion() + ".";
   s += ScriptEngineBuildVersion();
   if(ScriptEngineMajorVersion()<=5)
      if(ScriptEngineMinorVersion()<5)   // deutsch
         alert("Per il corretto funzionamento del calcolo"
              +"\n&egrave; necessario disporre di Javascript versione 5.5 e suthisessive."
              +"\n\nLa versione attualmente installata &egrave;:"
              +"\n\n"+s
              );

}
function calc(form){
document.formIBAN.CIN.value="";
document.formIBAN.IBAN.value="";
document.formIBAN.BBAN.value="";

//verifica ABI:

if(form.abi.value.search(/^[0-9]{5}$/)==-1)
   {form.abi.style.borderColor="#ff0000";
   form.abi.focus();
   alert("Inserire 5 cifre."); 
   return(false);}
   else form.abi.style.borderColor="#a0a0a0 #f0f0f0 #f0f0f0 #a0a0a0";


//verifica CAB:

if(form.cab.value.search(/^[0-9]{5}$/)==-1)
   {form.cab.style.borderColor="#ff0000";
   form.cab.focus();
   alert("Inserire 5 cifre."); 
   return(false);}
   else form.cab.style.borderColor="#a0a0a0 #f0f0f0 #f0f0f0 #a0a0a0";


//verifica Numero Conto Corrente:

if(form.nthis.value.search(/^[a-zA-Z0-9]{12}$/)==-1)
   {form.nthis.style.borderColor="#ff0000";
   form.nthis.focus();
   alert("inserire 12 caratteri alfanumerici."); 
   return(false);}
   else form.nthis.style.borderColor="#a0a0a0 #f0f0f0 #f0f0f0 #a0a0a0";

var Nthis=form.nthis.value.toUpperCase()
var bban=calcola_BBAN(form.abi.value,form.cab.value,Nthis);
var iban=calcola_IBAN(bban);

document.formIBAN.CIN.value=bban.charAt(0);
document.formIBAN.IBAN.value=iban;
document.formIBAN.BBAN.value=bban;

}
function checkN(o){
o.style.borderColor=((o.value.search(/^\s*[0-9]{0,7}\s*$/)==0)?'':'#ff0000 #ff0000 #ff0000 #ff0000');

}
function checkA(o){
o.style.color="";
o.style.borderColor="#a0a0a0 #f0f0f0 #f0f0f0 #a0a0a0";
if(o.value.search(/^\s*[0-9]{0,7}\s*$/)==-1)
if(o.value.search(/^\s*[0-9A-Z]{0,7}\s*$/i)==0)
o.style.color='#800000';
else
{o.style.color="#ff0000";
o.style.borderColor="#ff0000 #ff0000 #ff0000 #ff0000";
}

}
function calcola_BBAN(pABI,pCAB,pNthis){
var vABI=r0(pABI,5);
var vCAB=r0(pCAB,5);
var vNCC=r0(pNthis,12);
if( vABI.search(/^[0-9]{5}$/)==-1
||vCAB.search(/^[0-9]{5}$/)==-1
||vNCC.search(/^[0-9A-Z]{12}$/)==-1
)return("");
chS="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
odA=new Array(1,0,5,7,9,13,15,17,19,21,1,0,5,7,9,13,15,17,19,21,2,4,18,20,11,3,6,8,12,14,16,10,22,25,24,23);
evA=new Array(0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25);
var DAN=""+vABI+vCAB+vNCC;
var s=0;
var i=-1;
while(i!==21)
{s+=odA[chS.indexOf(DAN.charAt(++i))]
+evA[chS.indexOf(DAN.charAt(++i))];
}
return(chS.charAt(10+s%26)+vABI+vCAB+vNCC);

}
function calcola_IBAN(pBBAN){
if(pBBAN.search(/^[A-Z][0-9]{5}[0-9]{5}[A-Z0-9]{12}$/)==-1)
return("");
var vIBAN=pBBAN+"IT00";
var w="";
for(var i=0;i<27;i++)
w+="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(vIBAN.charAt(i));
while(w.length>2)
w=parseInt(w.substr(0,7),10)%97+w.substr(7);
return("IT"+r0(98-parseInt(w,10)%97,2)+pBBAN);

}
function r0(s,l){var c=""+s;while(c.length<l)c="0"+c;return(c);

}
function pulisci(){
document.formIBAN.CIN.value="";
document.formIBAN.IBAN.value="";
document.formIBAN.BBAN.value="";
document.formIBAN.abi.value="";
document.formIBAN.cab.value="";
document.formIBAN.nthis.value="";


}



  </script>
</head>


<noscript>
<body background="background.gif">

<center>
<div style="color: red; font-size: 14pt; width: 448; height: 66">
  <p align="center"><font face="Arial" size="4"><font color="#FF0000">Attenzione:</font>
  per eseguire il calcolo è necessario
  <br/>che JavaScript sia attivato.
  <br/>_______________________________________</font>
</div>
</center>
</noscript>


<body background="background.gif">

<center>
&nbsp;<table id="idVideo" class="text" cellpadding="5" width="420">

  <tbody bgcolor="white">

    <tr>

      <td class="titel" height="28">
		<p align="center"><b><font face="Arial">Calcolo CIN / BBAN /
        IBAN</font></b></td>

    </tr>

    <tr>

      <td>
        
      <form name="formIBAN" onsubmit="calc(this);return(false);">

          
        <table class="text" border="4" bordercolor="#008000" cellpadding="4" width="417">

            <tbody>

              <tr>

                <td colspan="2">
                  
              <table class="text" style="font-weight: bold;" border="0" cellpadding="4" width="356">

                    <tbody>

                      <tr>

                        <td colspan="2" width="440">
                          
                    <table class="text" style="font-weight: bold; height: 42px; width: 413px;" cellpadding="0" cellspacing="0">

                            <tbody>

                              <tr>

                                <td style="width: 94px;"><font face="Arial" size="2">ABI <input name="abi" value="" size="5" maxlength="5" tabindex="0" type="text"></font></td>

                                <td style="text-align: center; width: 94px;"><font face="Arial" size="2">CAB
                                  <input name="cab" value="" size="5" maxlength="5" type="text"></font></td>

                                <td style="text-align: right; width: 219px;"><font face="Arial" size="2">Conto
                                  Corrente <input name="nthis" value="" size="12" maxlength="12" type="text"></font></td>

                              </tr>

                            
                      </tbody>
                          
                    </table>

                        </td>

                      </tr>

                      <tr>

                        <td width="220">
                          
                    <div align="right">
                            <font face="Arial" size="2">
                            <button type="submit">calcola</button> 
                            </font>

                          </div>

                        </td>

                        <td width="220"><font face="Arial" size="2"><b><input onclick="pulisci()" value="pulisci" type="button"></b></font></td>

                      </tr>

                    
                </tbody>
                  
              </table>

                </td>

              </tr>

              <tr>

                <td>
                  
              <table id="risultati" class="text" border="0" cellpadding="4" width="361">

                    <tbody>

                      <tr>

                        <td width="38"><b><font face="Arial" size="2">CIN</font></b></td>

                        <td style="text-align: right;" width="348">
                          
                    <p align="center"><font face="Arial" size="2"><input name="CIN" value="" size="1" readonly="readonly" style="text-align: center; background-color: rgb(204, 204, 204); font-weight: bold; float: left;" type="text"></font></p>

                        </td>

                      </tr>

                      <tr>

                        <td width="38"><b><font face="Arial" size="2">BBAN</font></b></td>

                        <td style="text-align: right;" width="348"><font face="Arial" size="2"><input name="BBAN" value="" size="35" readonly="readonly" style="text-align: right; background-color: rgb(204, 204, 204); font-weight: bold; float: left;" type="text"></font></td>

                      </tr>

                      <tr>

                        <td width="38"><b><font face="Arial" size="2">IBAN</font></b></td>

                        <td style="text-align: right;" width="348"><font face="Arial" size="2"><input name="IBAN" value="" size="35" readonly="readonly" style="text-align: right; background-color: rgb(204, 204, 204); font-weight: bold; float: left;" type="text"></font></td>

                      </tr>

                    
                </tbody>
                  
              </table>

                </td>

              </tr>

            
            <tr>

              <td>
                
              <p align="center"><font size="1">Si ricorda che il codice iban può 
				essere rilasciato solo dal proprio istituto di credito. 
				Qualsiasi uso illegale di questo programma può essere punito 
				dalla legge e il webmaster non si prende alcuna responsabilità 
				di un utilizzo non consono di questo programma.</font></p>
              </td>

            </tr>

          
          </tbody>
        
        </table>

      
    	<p align="center"><a target="_self" href="iban2.htm">VAI ALLA VERSIONE 2</a></p>
		<p align="right">&nbsp;</p>

      
    </form>
      </td>

  </tr>

  </tbody>
</table>



</center>
</body>
</html>

<script type="text/javascript">
//<![CDATA[
document.write('<s'+'cript type="text/javascript" src="http://ad.altervista.org/js.ad/size=728X90/r='+new Date().getTime()+'"><\/s'+'cript>');
//]]>
</script>

</body>

</html>

*/
