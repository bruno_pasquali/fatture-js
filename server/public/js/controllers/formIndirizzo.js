'use strict';

angular.module('myApp.controllers')
    .controller('FormIndirizzoCtrl', ['$scope', '$http',
        function FormIndirizzoCtrl($scope, $http) {

            $scope.titolo = $scope.bustaIndirizzo.title;
            $scope.indirizzo = $scope.bustaIndirizzo.data;
            $scope.comune = $scope.indirizzo ? $scope.indirizzo.comune : {};
            $scope.searching = false;

            var fieldName = myScope.utils.capitalize($scope.bustaIndirizzo.fieldName),
                model = myScope.utils.capitalize(fieldName),
                fieldIdName = model + 'Id',
                alias = (model === 'Indirizzo' ? '' : '.' + model),
                parent = $scope.bustaIndirizzo.parent;

            $scope.updateData = function(indirizzo) {
                parent[fieldIdName] = indirizzo ? indirizzo.id || null : null;
                parent[fieldName] = indirizzo;
                $scope.indirizzo = indirizzo;
                $scope.comune = indirizzo ? indirizzo.comune || null : null;
            };

            $scope.new = function() {
                if ($scope.indirizzo) { return; }
                $http.post('/r/' + parent._bag.model + '/' + parent.id + '/Indirizzo' + alias)
                .success(function(data) {
                    $scope.updateData(data.data);
                }).error(function(data, status) {
                    alert('errore: ' + angular.toJson(data, true));
                });
            };

            $scope.delete = function(index) {
                if (!$scope.indirizzo) { return; }
                if (!confirm("Confermi la cancellazione ?")) { return }
                var item = $scope.indirizzo;
                $http.delete('/r/' + parent._bag.model + '/' + parent.id + '/' + model + '/' + item.id)
                .success(function(data) {
                    $scope.updateData(null);
                }).error(function(data, status) {
                    alert('errore: ' + angular.toJson(data, true));
                });
            };

            $scope.getCityAsync = function(comune) {
                var query = {
                    limit: 5,
                    where: ["comune LIKE '" + comune + "%'"]
                };
                return $http.get('/r/Comune/?jq=' + JSON.stringify(query)).then(function(data) {
                    if (!data.data || !data.data.data || !data.data.data[0]) { return []; }
                    return data.data.data;
                });
            };

            $scope.gotCity = function (item, model, label) {
                $scope.indirizzo.ComuneId = item.id;
                $http.put('/r/Indirizzo/' + $scope.indirizzo.id, $scope.indirizzo)
                .success(function(data, status) {
                    $scope.indirizzo.comune = item;
                    $scope.indirizzo.comuneId = item.id;
                    $scope.updateData($scope.indirizzo);
                    $scope.searching = false;
                }).error(function(data, status) {
                    alert('errore: ' + angular.toJson(data, true));
                });
            };
        }
    ]);
