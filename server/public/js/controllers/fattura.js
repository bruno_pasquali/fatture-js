'use strict';

/* Controllers */

angular.module('myApp.controllers')
    .controller('FatturaCtrl', ['$scope', '$routeParams', '$window', '$location', '$http',
        function FatturaCtrl($scope, $routeParams, $window, $location, $http) {
            var id = $routeParams.id;
            $scope.item = {};
            $scope.myScope = myScope;

            $http.get('/r/StoricoFattureEstintori/' + id).success(function(data) {
                $scope.item = new myScope.Fattura({fattura:data.data});
                $scope.item.imponibile = $scope.item.imponibile();
                $scope.item.totale = $scope.item.totale();
            }).error(function(data, status) {
                alert('errore: ' + angular.toJson(data, true));
            });

            $scope.gotoOrigin = function() {
                var query = {
                    where: ["AbbonamentoId='" + $scope.item.AbbonamentoId +"'"]
                };
                // $scope.updateStatus.updating = true;
                $http.get('/r/Cliente/' + $scope.item.ClienteId
                    +'/AbbonamentoEstintori/?jq=' + JSON.stringify(query))
                .success(function(data) {
                    // $scope.updateStatus.updating = false;
                    $location.path('/cliente/' + $scope.item.ClienteId + '/abbonamento/'+ $scope.item.AbbonamentoId);
                }).error(function(data, status) {
                    // $scope.updateStatus.updating = false;
                    $window.alert('Abbonamento originale non trovato\n probabilmente è stato cancellato');
                    // $scope.updateStatus.errorMessages.push('Abbonamento originale non trovato\n probabilmente è stato cancellato'
                    //     + ' (' + status + ') ' + data.message);
                    // $scope.updateStatus.errorMessages.push(angular.toJson(data, true));
                });
            };

        }
    ]);
