'use strict';

/* Controllers */

angular.module('myApp.controllers')
.controller('ElencoClientiCtrl', ['$scope', '$http', '$location', '$window',
    function ElencoClientiCtrl($scope, $http, $location, $window) {
        $scope.clientiAll = [];
        $http.get('/r/clientiView').success(function(data) {
            $scope.clientiAll = data.data || [];
            $scope.clienti = $scope.clientiAll.slice(0, 49);
        }).error(function(data, status) {
            alert('errore: ' + angular.toJson(data, true));
        });

        $scope.edit = function(id) {
            $location.path('/cliente/' + id);
        };

        $scope.new = function() {
            $http.post('/r/Cliente').success(function(data) {
                $scope.clienti.push(data.data);
                $location.path('/cliente/'+data.data.id);
            }).error(function(data, status) {
                $window.alert('errore in creazione cliente: \n' + data);
                // $scope.saving = false;
                // $scope.errorMessages.push('Salvataggio fallito, ' + ' (' + status + ') ' + data.message );
                // if (angular.isArray(data.data)) {
                //     for (var i = 0, l=data.data.length; i<l; i++) {
                //         $scope.errorMessages.push('   ' + i + ': ' +  data.data[i]);
                //     }
                // }
            });
        };
    }
]);
