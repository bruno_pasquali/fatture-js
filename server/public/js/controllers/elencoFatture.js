'use strict';

/* Controllers */

angular.module('myApp.controllers')
.controller('ElencoFattureCtrl', ['$scope', '$http', '$location', '$window',
    function ElencoFattureCtrl($scope, $http, $location, $window) {
        $scope.items = [];
        $http.get('/r/StoricoFattureEstintori').success(function(data) {
            $scope.itemsAll = data.data || [];
            $scope.items = $scope.itemsAll.slice(0, 49);
        }).error(function(data, status) {
            alert('errore: ' + angular.toJson(data, true));
        });

        $scope.view = function(id) {
            $location.path('/fattura/' + id);
        };
    }
]);
