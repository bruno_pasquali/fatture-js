'use strict';

/* Controllers */
angular.module('myApp.controllers')
    .controller('FormAbbonamentoCtrl', ['$scope', '$locale', '$route', '$http', '$location',
        function FormAbbonamentoCtrl($scope, $locale, $route, $http, $location) {
            // $scope.indirizzo = $scope.abbonamento.indirizzo || null;
            // $scope.comune = $scope.indirizzo.comune || null;
            $scope.dataEmiss = new Date($scope.abbonamento.dataEmissione);
            $scope.tipiPagamento = myScope.tipiPagamento;
            $scope.titoloContatti = 'Contatti manutenzione';
            $scope.contatti = $scope.abbonamento.contatto;
            $scope.indirizzo = $scope.abbonamento.indirizzo;
            // $scope.parent = $scope.abbonamento;
            $scope.bustaIndirizzo = {
                parent: $scope.abbonamento,
                data: $scope.indirizzo,
                fieldName: 'indirizzo',
                title: 'Indirizzo manutenzioni (se diverso dalla sede)'
            };

            $scope.fatture = [];
            var query = {
                where: ["AbbonamentoId='" + $scope.abbonamento.id + "'"]
            };
            $http.get('/r/StoricoFattureEstintori/?jq=' + JSON.stringify(query))
            .success(function(data) {
                $scope.fatture = data.data.sort(function(a, b){
                    return a.anno !== b.anno
                        ? a.anno - b.anno
                        : a.numero - b.numero;
                }).map(function(fattura, index){
                    return {
                        text: fattura.numero + ' / ' + fattura.anno,
                        anno: fattura.anno,
                        index: index,
                        id: fattura.id
                    };
                });
                $scope.fatturaSelezionata = $scope.fatture.slice(-1)[0];
            }).error(function(data, status) {
                $scope.fatture = [];
            });

            $scope.mesi = $locale.DATETIME_FORMATS.MONTH.slice(); //clone months
            $scope.mesi.unshift(''); //month 0 becomes empty, month 1 is January

            $scope.delete = function() {
                if (!confirm("Confermi la cancellazione ?")) { return }
                $scope.rimuoviAbbonamento($scope.index);
            };
            $scope.viewInvoice = function(fattura) {
                $location.path('/fattura/' + fattura.id);
            };
            $scope.newInvoice = function() {
                var year = (new Date()).getFullYear()
                var fatturaGiaPresente = $scope.fatture.some(function(fattura){
                    return fattura.anno === year;
                });
                if (fatturaGiaPresente) {
                    alert("C'è già una fattura per l'anno "+year);
                    return;
                }
                var fattura = new myScope.Fattura({cliente: $scope.cliente, abbonamento: $scope.abbonamento});
                $scope.updateStatus.updating = true;
                $http.post('/r/StoricoFattureEstintori', fattura)
                    .success(function(data, status) {
                        $scope.viewInvoice(data.data);

                    }).error(function(data, status) {
                        $scope.updateStatus.updating = false;
                        $scope.updateStatus.errorMessages.push('Creazione fattura fallita, ' + ' (' + status + ') ' + data.message);
                        $scope.updateStatus.errorMessages.push(
                          angular.toJson(data.data, true));
                    });

            };

            $scope.deleteInvoice = function(fattura) {
                if (!confirm("Confermi la cancellazione ?")) { return }
                $scope.updateStatus.updating = true;
                $http.delete('/r/StoricoFattureEstintori/' + fattura.id).success(function(data) {
                    $scope.updateStatus.updating = false;
                    $scope.fatture.splice(fattura.index, 1);
                }).error(function(data, status) {
                    $scope.updateStatus.updating = false;
                    $scope.updateStatus.errorMessages.push('Cancellazione fallita, ' + ' (' + status + ') ' + data.message);
                    $scope.updateStatus.errorMessages.push(angular.toJson(data, true));
                });
            };

        }
    ]);
