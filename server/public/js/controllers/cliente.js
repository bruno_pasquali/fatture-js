'use strict';

/* Controllers */

angular.module('myApp.controllers')
    .controller('ClienteCtrl', ['$scope', '$route', '$routeParams', '$location', '$http', '$interval',
        function ClienteCtrl($scope, $route, $routeParams, $location, $http, $interval) {
            // var timedCheck;
            // $scope.$on('$destroy', function() {
            //     // Make sure that the interval is destroyed too
            //     if (angular.isDefined(timedCheck)) {
            //         $interval.cancel(timedCheck);
            //         $scope.timedCheck = undefined;
            //     }
            // });
            // $scope.timedCheck = $interval(function() {

            // }, 5000);

            $scope.cliente = {};
            $scope.updateStatus = {
                updating: false,
                errorMessages: []
            };
            // $scope.abbonamenti = [];

            $http.get('/r/clienti/' + $routeParams.id).success(function(data) {
                (function(cliente, id, id_abbonamento) {
                    var abbs = cliente.abbonamentoEstintori.map(function(item) {
                        return new myScope.Abbonamento(item);
                    });
                    cliente.abbonamentoEstintori = abbs;
                    $scope.cliente = cliente;
                    $scope.cliente._bag.active = true;
                    $scope.parent = cliente;
                    $scope.conti = $scope.cliente.contoCorrente || {};
                    // $scope.abbonamenti = cliente.abbonamentoEstintori;
                    $scope.contatti = cliente.contatto;
                    // $scope.contattiManutenzione = cliente.contatto;
                    $scope.busteIndirizzi = [{
                        parent: cliente,
                        data: cliente.indirizzo,
                        fieldName: 'indirizzo',
                        title: ''
                    }, {
                        parent: cliente,
                        data: cliente.indirizzoFattura,
                        fieldName: 'indirizzoFattura',
                        title: 'per spedizione fattura (se diverso)'
                    }];
                    if (id_abbonamento) {
                        var a = _.find(abbs, function(a) {
                            return a.id.toString() === id_abbonamento.toString();
                        });
                        if (a) {a._bag.active = true};
                    }
                }(data.data || {}, $routeParams.id, $routeParams.id_abbonamento || null));
            }).error(function(data, status) {
                alert('errore: ' + angular.toJson(data, true));
            });

            $scope.deepSave = function(item) {
                $scope.updateStatus.updating = true;
                $http.put('/r/' + item._bag.model, item)
                    .success(function(data, status) {
                        $scope.updateStatus.updating = false;
                    }).error(function(data, status) {
                        $scope.updateStatus.updating = false;
                        $scope.updateStatus.errorMessages.push('Salvataggio fallito, ' + ' (' + status + ') ' + data.message);
                        if (angular.isArray(data.data)) {
                            for (var i = 0, l = data.data.length; i < l; i++) {
                                $scope.updateStatus.errorMessages.push('   ' + i + ': ' + data.data[i]);
                            }
                        }
                    });
            };
            $scope.nuovoAbbonamento = function(item) {
                $scope.updateStatus.updating = true;
                $http.post('/r/Cliente/' + item.id + '/AbbonamentoEstintori')
                    .success(function(data, status) {
                        $scope.updateStatus.errorMessages = [];
                        $scope.updateStatus.updating = false;
                        var abbs = $scope.cliente.abbonamentoEstintori;
                        abbs.push(new myScope.Abbonamento(data.data));
                        abbs[abbs.length - 1]._bag.active = true;
                        // $scope.abbonamenti.push(new myScope.Abbonamento(data.data));
                        //$route.reload();
                    }).error(function(data, status) {
                        $scope.updateStatus.updating = false;
                        $scope.updateStatus.errorMessages.push('Creazione abbonamento fallita, ' + ' (' + status + ') ' + data.message);
                        if (angular.isArray(data.data)) {
                            for (var i = 0, l = data.data.length; i < l; i++) {
                                $scope.updateStatus.errorMessages.push('   ' + i + ': ' + data.data[i]);
                            }
                        }
                    });
            };

            $scope.rimuoviAbbonamento = function(index) {
                var id = $scope.cliente.abbonamentoEstintori[index].id;
                $scope.updateStatus.updating = true;
                $http.delete('/r/abbonamenti/' + id).success(function(data) {
                    $scope.updateStatus.updating = false;
                    $scope.cliente.abbonamentoEstintori.splice(index, 1);
                    if (!$scope.cliente.abbonamentoEstintori.length) {
                        $scope.cliente._bag.active = true;
                    }
                }).error(function(data, status) {
                    $scope.updateStatus.updating = false;
                    $scope.updateStatus.errorMessages.push('Cancellazione fallita, ' + ' (' + status + ') ' + data.message);
                    $scope.updateStatus.errorMessages.push(angular.toJson(data, true));
                });
            };

            $scope.cleanErrorMessages = function(item) {
                $scope.updateStatus.errorMessages = [];
            }
            $scope.delete = function() {
                var message = $scope.cliente.abbonamentoEstintori.length > 0 ? "Confermi la cancellazione del cliente,\n  e di TUTTI i suoi abbonamenti ?" : "Confermi la cancellazione del cliente ?";
                if (!confirm(message)) {
                    return
                }
                $scope.updateStatus.updating = true;
                $http.delete('/r/clienti/' + $scope.cliente.id)
                    .success(function(data) {
                        $scope.updateStatus.updating = false;
                        $location.path('/elencoClienti');
                    }).error(function(data, status) {
                        $scope.updateStatus.updating = false;
                        $scope.updateStatus.errorMessages.push('Cancellazione fallita, ' + ' (' + status + ') ' + data.message);
                        $scope.updateStatus.errorMessages.push(angular.toJson(data, true));
                    });
            };
        }
    ]);
