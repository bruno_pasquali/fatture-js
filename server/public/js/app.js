'use strict';

// window.ngGrid.i18n['it'] = {
//     ngAggregateLabel: 'elemento',
//     ngGroupPanelDescription: 'Trascina qui il titolo di una colonna per raggruppare.',
//     ngSearchPlaceHolder: 'Cerca...',
//     ngMenuText: 'Seleziona le colonne:',
//     ngShowingItemsLabel: 'Elementi visualizzati:',
//     ngTotalItemsLabel: 'Elementi totali:',
//     ngSelectedItemsLabel: 'Elementi selezionati:',
//     ngPageSizeLabel: 'Elementi per pagina:',
//     ngPagerFirstTitle: 'Prima pagina',
//     ngPagerNextTitle: 'Prossima pagina',
//     ngPagerPrevTitle: 'Pagina precedente',
//     ngPagerLastTitle: 'Ultima pagina'
// };

// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'ngRoute',
  'ui.utils',   //non usato ?
  'ui.bootstrap',

  // 'ngGrid',
  'ngTable',

  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/main', {templateUrl: 'partials/main.html', controller: 'MainCtrl'});
  $routeProvider.when('/cliente/:id',
    {
      templateUrl: 'partials/cliente.html'
      , controller: 'ClienteCtrl'
      // , resolve: {
      //   data: $http.get('/r/clienti/' + $routeParams.id)
      // }
    }
  );
  $routeProvider.when('/cliente/:id/abbonamento/:id_abbonamento',
    {templateUrl: 'partials/cliente.html', controller: 'ClienteCtrl'});
  $routeProvider.when('/fattura/:id',
    {templateUrl: 'partials/fattura.html', controller: 'FatturaCtrl'});
  $routeProvider.when('/elencoClienti',
    {templateUrl: 'partials/ElencoClienti.html', controller: 'ElencoClientiCtrl'});
  $routeProvider.when('/elencoFatture',
    {templateUrl: 'partials/ElencoFatture.html', controller: 'ElencoFattureCtrl'});
  $routeProvider.when('/elencoComuni',
    {templateUrl: 'partials/elencoComuni.html', controller: 'ElencoComuniCtrl'});
  $routeProvider.otherwise({redirectTo: '/main'});
}]);
