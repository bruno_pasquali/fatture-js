require 'csv'

Sequel.migration do
    transaction
	up do
		puts "Creazione comuni"
	    create_table(:Comune) do       
	      primary_key :id   
	      Fixnum	 :istat, :fixed=>true, :size=>6 
	      String     :comune, :size=>40             
	      String     :CAP, :fixed=>true, :size=>6                 
	      String     :provincia, :fixed=>true, :size=>2       
	      String     :regione, :fixed=>true, :size=>3       
	      String     :prefisso, :fixed=>true, :size=>4                 
	      String     :codiceFisco, :fixed=>true, :size=>4                 
		  DateTime	 :createdAt
		  DateTime	 :updatedAt
	      index [:comune]
	      index [:CAP]
	    end
		# DB_CSV = Sequel.ado(:conn_string=>'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=.;Extended Properties="text;HDR=Yes;FMT=Delimited";')
		# DB_CSV[:"lc1.txt"].select(:Istat, :Comune, :Provincia, :Regione, :Prefisso, :CAP, :CodFisco).each do |c|
		CSV.foreach("lc1.txt", {:headers => true, :header_converters => :symbol, :col_sep => ";", :row_sep => :auto}) do |c|
			self[:Comune].insert(
				:istat => c[:istat], 
				:comune => c[:comune], 
				:provincia => c[:provincia], 
				:regione => c[:regione],
				:prefisso => c[:prefisso],
				:CAP => c[:cap],
				:codiceFisco => c[:codfisco]
			)
		end
	end
	down do
 	   drop_table :Comune
 	end
end
