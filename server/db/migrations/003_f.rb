require 'dbf'

$fix = [
	#["", [, "", ""]],
	["gromlongo di palazzago", [24030, "Palazzago", "Gromlongo"]],
	["gavardo ?", [25085, "Gavardo", ""]],
	["comezzano cizzago", [25030, "Comezzano-Cizzago", ""]],
	["gardone v.t.", [25063, "Gardone Val Trompia", ""]],
	["manerba sul garda", [25080, "Manerba del Garda", ""]],
	["ronco di gussago", [25064, "Gussago", "Ronco"]],
	["s. zeno naviglio", [25010, "San Zeno Naviglio", ""]],
	["collio di vobarno", [25079, "Vobarno", "Collio"]],
	["borgo poncarale", [25020, "Poncarale", "Borgo Poncarale"]],
	["borgo s. giacomo", [25022, "Borgo San Giacomo", ""]],
	["camignone", [25050, "Passirano", "Camignone"]],
	["clusane", [25049, "Iseo", "Clusane"]],
	["pilzone", [25049, "Iseo", "Pilzone"]],
	["cortefranca", [25040, "Corte Franca", "Nigoline"]],
	["lodetto", [25038, "Rovato", "Lodetto"]],
	["borgonato",[25040, "Corte Franca", "Borgonato"]],
	["colombaro",[25040, "Corte Franca", "Colombaro"]],
	["timoline",[25040, "Corte Franca", "Timoline"]],
	["nigoline",[25040, "Corte Franca", "Nigoline"]],
	["tagliuno",[24060, "Castelli Calepio", "Tagliuno"]],
	["gazzane di preseglie",[25070, "Preseglie", "Gazzane"]],
	["adrara s. martino",[24060, "Adrara San Martino", ""]],
	["adrara san martino",[24060, "Adrara San Martino", ""]],
	["adrara s/m",[24060, "Adrara San Martino", ""]],
	["adrara",[24060, "Adrara San Martino", ""]],
	["villa di erbusco",[25030, "Erbusco", "Villa"]],
	["villa d'erbusco",[25030, "Erbusco", "Villa"]],
	["zocco ",[25030, "Erbusco", "Zocco"]],
	["palazzolo s/o",[25036, "Palazzolo sull'Oglio", ""]],
	["san pancrazio",[25036, "Palazzolo sull'Oglio", "San Pancrazio"]],
	["s. pancrazio",[25036, "Palazzolo sull'Oglio", "San Pancrazio"]],
	["cazzago s",[25046, "Cazzago San Martino", ""]],
	["adro, torbiato",[25030, "Adro", "Torbiato"]],
	["cividino",[24060, "Castelli Calepio", "Cividino"]],
	["quintano di castelli calepio",[24060, "Castelli Calepio", "Quintano"]],
	["ferrara",["4412x", "Ferrara", ""]],
]

def fixComune cap, comune
	c = comune.downcase
	if c[0,11] == "provaglio d"
		return [25050, "Provaglio d'Iseo", ""]
	end
	$fix.each { |match|
		return match[1] if match[0] == c[0,match[0].length]
	}
	return [cap, comune, ""]
end

def allCap s
	return s.strip.downcase.split(' ').map { |w| if (w == "di" or w == "del") then w else w.capitalize end}.join(' ')
end

def trovaBanca ds, abi, cab
	#b = ds.filter(:CAB => cab).filter(:ABI => abi)
	b = ds.where{(:CAB == cab) & (:ABI == abi)}
	trovata = b.count > 0
	id = b.first[:id] if trovata
	return [trovata, id]
end

def trovaComune ds, row, symCap, symCitta
	capOK = ""
	localita = ""
	comune = allCap((row[symCitta].to_s)[0,6])
	cap = row[symCap].to_s
	info = cap.to_s + "-" + row[symCitta].to_s
	#ricerca con CAP
	dsComune = ds[:Comune].filter(:cap => cap).filter(:comune.like(comune+'%'))
	if dsComune.count == 0
		comune = allCap((row[symCitta].to_s)[0,15])
		#ricerca senza CAP
		dsComune = self[:Comune].filter(:comune.like(comune+'%'))
	end
	if dsComune.count == 0 or dsComune.count > 1
		#forse comune errato: sistema e cerca ancora
		cap, comune, localita = fixComune cap, (allCap (row[symCitta].to_s))
		dsComune = self[:Comune].filter(:cap => cap).filter(:comune.like(comune))
	end
	if dsComune.count > 1
		puts ("err: + comuni " + row[:ANNOFATT].to_s + "." + row[:NUMEFATT].to_s + ": " + info)
		dsComune.each{ |c| puts c }
	end
	if dsComune.count == 0
		puts ("err: 0 comuni " + row[:ANNOFATT].to_s + "." + row[:NUMEFATT].to_s + ": " + info)
	else
		#puts ("inf: " + row[:ANNOFATT].to_s + "." + row[:NUMEFATT].to_s + ": " + info +  " => " + dsComune.first[:CAP].to_s + "-" + dsComune.first[:comune].to_s)
		idComune = dsComune.first[:id].to_s
		capOK = dsComune.first[:CAP].to_s
	end
	idComune = nil if idComune == ""
	return [idComune, capOK, localita]
end

def CalcolaTotale dsFattura
	#return ROUND((COSTSPES+COSTO)*((IVA-RITENUTA+100)/100),2)
end

def TipoPagamento (isBonifico, isRimessaDiretta, isRiba)
	return 203 if isBonifico
	return 202 if isRimessaDiretta
	return 201 if isRiba
	return 0
end

$fatturaPrecedente = Array.new(1000, false)
$maxFattura = 0

Sequel.migration do
	transaction
	up do
		puts __FILE__
		def addData row, isCurrentYear
			if isCurrentYear
				$fatturaPrecedente[row[:NUMEPREC].to_i] = true
			else
				return if $fatturaPrecedente[row[:NUMEFATT]]
			end
			$maxFattura = $maxFattura.succ
			idCliAbb = $maxFattura

			isRimessaDiretta = ((row[:RIMESSAD].to_s== "S") || (row[:BANCA].to_s.downcase.include? "rimessa"))
			isBonifico = ((row[:BONIFICO].to_s== "S") || (row[:BANCA].to_s.downcase.include? "bonifico"))
			isRiba = ( !isBonifico && !isRimessaDiretta && (row[:BANCA].to_s.strip != "") )


			idComune, cap, localita = trovaComune self, row, :CAP, :CITTA
			idIndirizzoRagioneSociale = self[:Indirizzo].insert(
				:indirizzo => row[:INDIRIZ1].to_s + " " + row[:INDIRIZ2].to_s,
				:CAP => cap,
				:localita => localita,
				:ComuneId => idComune
			)
			idIndirizzoSpedizioneFattura = nil
			if row[:CITTAAMM].to_s.rstrip != ""
				idComune, cap, localita = trovaComune self, row, :CAPAMM, :CITTAAMM
				idIndirizzoSpedizioneFattura = self[:Indirizzo].insert(
					:indirizzo => row[:INDIAMM1].to_s + " " + row[:INDIAMM2].to_s,
					:CAP => cap,
					:localita => localita,
					:ComuneId => idComune
				)
			end
			idIndirizzoAbbonamento = nil
			if row[:CITTACON].to_s.rstrip != ""
				idComune, cap, localita = trovaComune self, row, :CAPCON, :CITTACON
				idIndirizzoAbbonamento = self[:Indirizzo].insert(
					:indirizzo => row[:INDICON1].to_s + " " + row[:INDICON2].to_s,
					:CAP => cap,
					:localita => localita,
					:ComuneId => idComune
				)
			end
			idCliente = self[:Cliente].insert(
				:numero => idCliAbb,
                :ragioneSociale1 => row[:RAGSOCI1].to_s,
                :ragioneSociale2 => row[:RAGSOCI2].to_s,
				:codiceFiscale => row[:CODFISCA],
				:partitaIVA => row[:PARTIIVA],
				:IndirizzoId => idIndirizzoRagioneSociale,
				:IndirizzoFatturaId => idIndirizzoSpedizioneFattura,
				:note => row[:NOTE1].to_s + " " + row[:NOTE2].to_s
			)
			if row[:TELEFON1].to_s.rstrip != ""
				idContatto = self[:Contatto].insert(
					:contatto => row[:TELEFON1].to_s,
					:tipo => 101
				)
                self[:ClienteContatto].insert(
                    :ClienteId => idCliente,
                    :ContattoId => idContatto,
                )
			end
			if row[:TELEFON2].to_s.rstrip != ""
                idContatto = self[:Contatto].insert(
					:contatto => row[:TELEFON2].to_s,
					:tipo => 101
				)
                self[:ClienteContatto].insert(
                    :ClienteId => idCliente,
                    :ContattoId => idContatto,
                )
			end
            if row[:FAX].to_s.rstrip != ""
                idContatto = self[:Contatto].insert(
					:contatto => row[:FAX].to_s,
					:tipo => 102
				)
                self[:ClienteContatto].insert(
                    :ClienteId => idCliente,
                    :ContattoId => idContatto,
                )
			end
			# abi = row[:ABI] || ""
			# cab = row[:CAB] || ""
			if isRiba
				#trovata, idContoCorrente = trovaBanca self[:idContoCorrente], abi, cab
				#if not trovata
					#idContoCorrente =
					self[:ContoCorrente].insert(
						:ABI => row[:ABI],
						:CAB => row[:CAB],
						:banca => row[:BANCA],
						:filiale => row[:FILIALE].to_s,
						:contoCorrente => row[:CONTOCOR].to_s,
						:ClienteId => idCliente
					)
				#end
				#puts trovata.to_s + " " + abi
			end

			#p row[:DATAEMIS]
			idAbbonamento = self[:AbbonamentoEstintori].insert(
				:numero => idCliAbb,   								#!!!!!!!!
				:ClienteId => idCliente,
				:IndirizzoId => idIndirizzoAbbonamento,
				:note  => row[:NOTE1].to_s + " " + row[:NOTE2].to_s,
				:seDisdetta => (row[:DISDETTA].to_s == "S"),
				:seFatturareAGennaio => (row[:GENNAIO].to_s== "S"),     #:GENNAIO    #---
				# :seRimessaDiretta => isRimessaDiretta,
				# :seBonifico => isBonifico,
				:tipoPagamento => TipoPagamento(isBonifico, isRimessaDiretta, isRiba),
				:numeroEstintori => row[:NUMESTIN],
				:descrizionePagamento => row[:PAGAMENT].to_s,
				:costo => row[:COSTO],  #ver. precisione
				:IVA => row[:IVA],
				:descrizioneSpese => row[:DESCSPES].to_s,
				:costoSpese => row[:COSTSPES],
				:giornoEmissione => row[:DATAEMIS].mday,
				:meseEmissione => row[:DATAEMIS].month,
				:dataFatturaFineMese => (row[:DSCADGGP] != "S"),
				#:dataPagamento => row[:DATAPAGA],
				:giorniPagamento => row[:GGPAGAME],
				:ritenuta => row[:RITENUTA],
				#:contoCorrente => row[:CONTOCOR].to_s
				#:IBAN => calcolaIBAN()
			)
			#self[:AbbonamentoEstintori].where(:id => idAbbonamento).each { |row| p row[:dataEmissione] }

			self[:StoricoFattureEstintori].insert(
				:ClienteId => idCliente,
				:AbbonamentoId => idAbbonamento,
				:anno => row[:ANNOFATT],
				:numero => row[:NUMEFATT],
				:numeroPrecedente => row[:NUMEPREC],
                :ragioneSociale1 => row[:RAGSOCI1].to_s,
                :ragioneSociale2 => row[:RAGSOCI2].to_s,
				:codiceFiscale => row[:CODFISCA],
				:partitaIVA => row[:PARTIIVA],

				:indirizzo => row[:INDIRIZ1].to_s + " " + row[:INDIRIZ2].to_s,
				:comune => row[:CITTA].to_s,
				:CAP => row[:CAP],
				:provincia => row[:PROVINCI].to_s,

				:indirizzoManutenzione => row[:INDICON1].to_s + " " + row[:INDICON2].to_s,
				:comuneManutenzione => row[:CITTACON].to_s,
				:CAPManutenzione => row[:CAPCON],
				:provinciaManutenzione => row[:PROVICON].to_s,

				:indirizzoFattura => row[:INDIAMM1].to_s + " " + row[:INDIAMM2].to_s,
				:comuneFattura => row[:CITTAAMM].to_s,
				:CAPFattura => row[:CAPAMM],
				:provinciaFattura => row[:PROVIAMM].to_s,

				:note  => row[:NOTE1].to_s + " " + row[:NOTE2].to_s,
				:descrizionePagamento => row[:PAGAMENT].to_s,
				:costo => row[:COSTO],  #ver. precisione
				:numeroEstintori => row[:NUMESTIN],
				:IVA => row[:IVA],
				:descrizioneSpese => row[:DESCSPES].to_s,
				:costoSpese => row[:COSTSPES],
				:dataEmissione => row[:DATAEMIS],
				:dataFatturaFineMese => (row[:DSCADGGP] != "S"),
				# :dataPagamento => row[:DATAPAGA],
				:giorniPagamento => row[:GGPAGAME],
				:ritenuta => row[:RITENUTA],
				:ABI => row[:ABI],
				:CAB => row[:CAB],
				:banca => row[:BANCA],
				:filiale => row[:FILIALE].to_s,
				:contoCorrente => row[:CONTOCOR].to_s,
				:seDisdetta => (row[:DISDETTA].to_s == "S"),
				:tipoPagamento => TipoPagamento(isBonifico, isRimessaDiretta, isRiba)
				# :seRimessaDiretta => isRimessaDiretta,
				# :seBonifico => isBonifico,
				#:IBAN => calcolaIBAN()
			)
		end
		# DB_DBF = Sequel.ado(:conn_string=>'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=.;Extended Properties=dBASE IV;User ID=Admin;Password=')
		DB_DBF = DBF::Table.new("f.dbf")
		DB_DBF.find(:all, :ANNOFATT => 2014) do |row|
        # DB_DBF[:F].filter(:ANNOFATT => 2013).each do |row|
			addData row, true
		end
		DB_DBF.find(:all, :ANNOFATT => 2013) do |row|
		# DB_DBF[:F].filter(:ANNOFATT => 2012).each do |row|
			addData row, false
		end
		time = Time.now
		self.tables.each do |table|
			#puts table
            next if "#{table}" == "schema_info"
            next if not self[table].columns.include? :createdAt
			from(table).update(:createdAt=>time)
			from(table).update(:updatedAt=>time)
		end
	end
=begin
validates_each :iban do | record, attr, value |
    record.errors.add attr, 'IBAN is mandatory' and next if value.blank?
    # IBAN code should start with country code (2letters)
    record.errors.add attr, 'Country code is missing from the IBAN code' and next unless value.to_s =~ /^[A-Z]{2}/i
    iban = value.gsub(/[A-Z]/) { |p| (p.respond_to?(:ord) ? p.ord : p[0]) - 55 }
    record.errors.add attr, 'Invalid IBAN format' unless (iban[6..iban.length-1].to_s+iban[0..5].to_s).to_i % 97 == 1
  end
=end
	down do
		def downAction table
		  puts "Emptying: " + table.to_s
		  self[table].delete
		end
		puts __FILE__
	    downAction :StoricoFattureEstintori
      downAction :AbbonamentoEstintoriContatto
      downAction :ClienteContatto
	    downAction :AbbonamentoEstintori
	    downAction :ContoCorrente
	    downAction :Cliente
	    downAction :Indirizzo
	    downAction :Contatto
	end
end
