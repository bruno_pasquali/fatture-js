require 'sequel'

$sequelizeMap = {
  'boolean' => 'DataTypes.BOOLEAN'
}

def sequelizeType db_type
  return "{ type: #{$sequelizeMap[db_type]} " if $sequelizeMap.include? db_type
  return "{ type: '#{db_type}' "
end

def dumpSequelizeModel db, fass
    db.tables.each do |table|
      next if "#{table}" === "schema_info"
      fileName = "#{table}.model.js"
      puts fileName
      fks = {}      #[{:columns=>[:column1], :table=>:referenced_table, :key=>[:referenced_column1]}]
      if db.foreign_key_list(table).count > 0
          fass.write("m.#{table}");
          db.foreign_key_list(table).each do |fk|
              puts fk
              c = "#{fk[:columns][0]}"
              fks[c] = "#{fk[:table]}"
              fass.write("\n    .belongsTo(m.#{fk[:table]},)")
          end
          fass.write(";\n")
      end
      # puts "..................."
      # puts fks
      File.delete(fileName) if File.exist?(fileName)
        schema = db.schema(table)
        #puts schema
        #schema.each { |field| puts field }
        File.open(fileName, 'w') do |f|
          f.write("//\n")
          f.write("//Automatic generated file: do not modify directly\n")
          f.write("//\n")
          f.write("module.exports = function(sequelize, DataTypes) {\n")
          f.write("    return sequelize.define('#{table}', {\n")
          schema.each { |field|
                name = field[0].to_s
                next if (name == "id" or name == "updatedAt" or name == "createdAt")
                # foreign keys definite nelle relazioni di sequelize ??
                next if fks[name] != nil
                opts = field[1]
                f.write("        #{name}:")
                #f.write("{ type: '#{opts[:db_type]}' ")
                f.write(sequelizeType opts[:db_type])
                f.write(", allowNull: false") if not opts[:allow_null]
                f.write(", primaryKey: true") if opts[:primary_key]
                f.write(", defaultValue: #{opts[:default]}") if opts[:default] != nil
                # mai eseguita: vedi nota sopra
                f.write(", references: '#{fks[name]}', referencesKey: 'id'") if fks[name] != nil
                f.write(" },\n")
          }
          f.write("    }, {")
          f.write("timestamps: false, ") if not db[table].columns.include? :createdAt
          f.write("freezeTableName: true});")
          f.write("\n};")
        end
    end
end

associationsJsFile = "associazioni.txt"
File.delete(associationsJsFile) if File.exist?(associationsJsFile)
storage = File.join(File.expand_path(File.dirname(__FILE__)), ".", "db.sqlite3")
Sequel.sqlite(storage) { |db|
    File.open(associationsJsFile, 'w') { |fass|
      dumpSequelizeModel db, fass
    }
}
