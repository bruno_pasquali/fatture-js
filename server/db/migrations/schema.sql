CREATE TABLE `schema_info` (`version` INT DEFAULT(0) NOT NULL);

CREATE TABLE `Comune` (
	`id` INT NOT NULL PRIMARY KEY AUTOINCREMENT
	,`Istat` INT
	,`comune` VARCHAR(30)
	,`CAP` CHAR(6)
	,`provincia` CHAR(2)
	,`regione` CHAR(3)
	,`prefisso` CHAR(4)
	,`codiceFisco` CHAR(4)
	,`createdAt` TIMESTAMP
	,`updatedAt` TIMESTAMP
	);

CREATE TABLE sqlite_sequence (
	NAME
	,seq
	);

CREATE INDEX `Comune_comune_index` ON `Comune` (`comune`);

CREATE INDEX `Comune_CAP_index` ON `Comune` (`CAP`);

CREATE TABLE `StoricoFattureEstintori` (
	`id` INT NOT NULL PRIMARY KEY AUTOINCREMENT
	,`ClienteId` INT
	,`AbbonamentoId` INT
	,`anno` INT NOT NULL
	,`numero` INT NOT NULL
	,`numeroPrecedente` INT
	,`ragioneSociale` VARCHAR(255)
	,`codiceFiscale` CHAR(16)
	,`partitaIVA` CHAR(12)
	,`indirizzo` VARCHAR(255)
	,`comune` VARCHAR(30)
	,`CAP` CHAR(6)
	,`provincia` CHAR(2)
	,`indirizzoSpedizione` VARCHAR(255)
	,`comuneSpedizione` VARCHAR(30)
	,`CAPSpedizione` CHAR(6)
	,`provinciaSpedizione` CHAR(2)
	,`indirizzoManutenzione` VARCHAR(255)
	,`comuneManutenzione` VARCHAR(30)
	,`CAPManutenzione` CHAR(6)
	,`provinciaManutenzione` CHAR(2)
	,`seRimessaDiretta` boolean
	,`descrizionePagamento` VARCHAR(20) DEFAULT('')
	,`note` VARCHAR(255) DEFAULT('')
	,`costo` NUMERIC(10, 2)
	,`numeroEstintori` INT
	,`IVA` FLOAT
	,`descrizioneSpese` VARCHAR(255) DEFAULT('')
	,`costoSpese` NUMERIC(10, 2)
	,`dataEmissione` DATE
	,`dataPagamento` DATE
	,`seNotaAccredito` boolean
	,`giorniPagamento` INT
	,`seBonifico` boolean
	,`ritenuta` FLOAT
	,`banca` VARCHAR(50)
	,`filiale` VARCHAR(20)
	,`IBAN` VARCHAR(28)
	,`createdAt` TIMESTAMP
	,`updatedAt` TIMESTAMP
	);

CREATE INDEX `StoricoFattureEstintori_anno_numero_index` ON `StoricoFattureEstintori` (
	`anno`
	,`numero`
	);

CREATE INDEX `StoricoFattureEstintori_ragioneSociale_index` ON `StoricoFattureEstintori` (`ragioneSociale`);

CREATE INDEX `StoricoFattureEstintori_dataPagamento_index` ON `StoricoFattureEstintori` (`dataPagamento`);

CREATE TABLE `Cliente` (
	`id` INT NOT NULL PRIMARY KEY AUTOINCREMENT
	,`numero` INT NOT NULL UNIQUE
	,`ragioneSociale` VARCHAR(255)
	,`codiceFiscale` CHAR(16)
	,`partitaIVA` CHAR(12)
	,`note` VARCHAR(255) DEFAULT('')
	,`createdAt` TIMESTAMP
	,`updatedAt` TIMESTAMP
	,`IndirizzoFatturaId` INT REFERENCES `Indirizzo`
	,`IndirizzoId` INT REFERENCES `Indirizzo`
	);

CREATE INDEX `Cliente_numero_index` ON `Cliente` (`numero`);

CREATE INDEX `Cliente_ragioneSociale_index` ON `Cliente` (`ragioneSociale`);

CREATE TABLE `AbbonamentoEstintori` (
	`id` INT NOT NULL PRIMARY KEY AUTOINCREMENT
	,`numero` INT NOT NULL UNIQUE
	,`seRimessaDiretta` boolean
	,`descrizionePagamento` VARCHAR(20) DEFAULT('')
	,`note` VARCHAR(255) DEFAULT('')
	,`costo` NUMERIC(10, 2)
	,`numeroEstintori` INT
	,`IVA` FLOAT
	,`descrizioneSpese` VARCHAR(255) DEFAULT('')
	,`costoSpese` NUMERIC(10, 2)
	,`seDisdetta` boolean
	,`dataPagamento` DATE
	,`giorniPagamento` INT
	,`seFatturareAGennaio` boolean
	,`seBonifico` boolean
	,`ritenuta` FLOAT
	,`contoCorrente` CHAR(12)
	,`IBAN` CHAR(27)
	,`tipo` INT
	,`createdAt` TIMESTAMP
	,`updatedAt` TIMESTAMP
	,`IndirizzoId` INT REFERENCES `Indirizzo`
	,`BancaId` INT REFERENCES `Banca`
	,`ClienteId` INT REFERENCES `Cliente`
	);

CREATE TABLE `Banca` (
	`id` INT NOT NULL PRIMARY KEY AUTOINCREMENT
	,`ABI` CHAR(5)
	,`CAB` CHAR(5)
	,`banca` VARCHAR(50)
	,`filiale` VARCHAR(20)
	,`createdAt` TIMESTAMP
	,`updatedAt` TIMESTAMP
	);

CREATE INDEX `Banca_ABI_CAB_index` ON `Banca` (
	`ABI`
	,`CAB`
	);

CREATE INDEX `Banca_banca_filiale_index` ON `Banca` (
	`banca`
	,`filiale`
	);

CREATE TABLE `Indirizzo` (
	`id` INT NOT NULL PRIMARY KEY AUTOINCREMENT
	,`indirizzo` VARCHAR(255)
	,`localita` VARCHAR(30)
	,`note` VARCHAR(255) DEFAULT('')
	,`CAP` CHAR(6)
	,`createdAt` TIMESTAMP
	,`updatedAt` TIMESTAMP
	,`ComuneId` INT REFERENCES `Comune`
	);

CREATE INDEX `Indirizzo_indirizzo_index` ON `Indirizzo` (`indirizzo`);

CREATE TABLE `Telefono` (
	`id` INT NOT NULL PRIMARY KEY AUTOINCREMENT
	,`numero` VARCHAR(25)
	,`descrizione` VARCHAR(50) DEFAULT('')
	,`tipo` INT
	,`createdAt` TIMESTAMP
	,`updatedAt` TIMESTAMP
	);

CREATE INDEX `Telefono_numero_index` ON `Telefono` (`numero`);

CREATE TABLE `ClienteTelefono` (
	`createdAt` TIMESTAMP
	,`updatedAt` TIMESTAMP
	,`ClienteId` INT REFERENCES `Cliente`
	,`TelefonoId` INT REFERENCES `Telefono`
	);

CREATE INDEX `ClienteTelefono_ClienteId_TelefonoId_index` ON `ClienteTelefono` (
	`ClienteId`
	,`TelefonoId`
	);

CREATE TABLE `AbbonamentoEstintoriTelefono` (
	`createdAt` TIMESTAMP
	,`updatedAt` TIMESTAMP
	,`AbbonamentoEstintoriId` INT REFERENCES `AbbonamentoEstintori`
	,`TelefonoId` INT REFERENCES `Telefono`
	);

CREATE INDEX `AbbonamentoEstintoriTelefono_AbbonamentoEstintoriId_TelefonoId_index` ON `AbbonamentoEstintoriTelefono` (
	`AbbonamentoEstintoriId`
	,`TelefonoId`
	);
