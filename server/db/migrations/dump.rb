Sequel.migration do
  change do
    create_table(:Comune, :ignore_index_errors=>true) do
      primary_key :id
      Integer :istat
      String :comune, :size=>40
      String :CAP, :size=>6, :fixed=>true
      String :provincia, :size=>2, :fixed=>true
      String :regione, :size=>3, :fixed=>true
      String :prefisso, :size=>4, :fixed=>true
      String :codiceFisco, :size=>4, :fixed=>true
      DateTime :createdAt
      DateTime :updatedAt
      
      index [:CAP]
      index [:comune]
    end
    
    create_table(:Contatto) do
      primary_key :id
      String :contatto, :default=>"", :size=>255
      String :descrizione, :default=>"", :size=>50
      Integer :tipo, :default=>0
      DateTime :createdAt
      DateTime :updatedAt
    end
    
    create_table(:StoricoFattureEstintori, :ignore_index_errors=>true) do
      primary_key :id
      Integer :ClienteId, :default=>0
      Integer :AbbonamentoId, :default=>0
      Integer :anno, :default=>0, :null=>false
      Integer :numero, :default=>0, :null=>false
      Integer :numeroPrecedente, :default=>0
      String :ragioneSociale1, :default=>"", :size=>100
      String :ragioneSociale2, :default=>"", :size=>100
      String :codiceFiscale, :default=>"", :size=>16, :fixed=>true
      String :partitaIVA, :default=>"", :size=>11, :fixed=>true
      String :indirizzo, :default=>"", :size=>150
      String :localita, :default=>"", :size=>150
      String :comune, :default=>"", :size=>40
      String :CAP, :default=>"", :size=>6, :fixed=>true
      String :provincia, :default=>"", :size=>2, :fixed=>true
      String :indirizzoFattura, :default=>"", :size=>150
      String :localitaFattura, :default=>"", :size=>150
      String :comuneFattura, :default=>"", :size=>40
      String :CAPFattura, :default=>"", :size=>6, :fixed=>true
      String :provinciaFattura, :default=>"", :size=>2, :fixed=>true
      String :indirizzoManutenzione, :default=>"", :size=>150
      String :localitaManutenzione, :default=>"", :size=>150
      String :comuneManutenzione, :default=>"", :size=>40
      String :CAPManutenzione, :default=>"", :size=>6, :fixed=>true
      String :provinciaManutenzione, :default=>"", :size=>2, :fixed=>true
      String :note, :default=>"", :size=>255
      String :descrizionePagamento, :default=>"", :size=>30
      BigDecimal :costo, :default=>BigDecimal.new("0.0"), :size=>[10, 2]
      Integer :numeroEstintori, :default=>0
      Float :IVA, :default=>0.0
      String :descrizioneSpese, :default=>"", :size=>40
      BigDecimal :costoSpese, :default=>BigDecimal.new("0.0"), :size=>[10, 2]
      DateTime :dataEmissione
      DateTime :dataPagamento
      TrueClass :dataFatturaFineMese, :default=>true
      TrueClass :seNotaAccredito, :default=>false
      TrueClass :seDisdetta, :default=>false
      Integer :giorniPagamento, :default=>0
      Integer :tipoPagamento, :default=>0
      Float :ritenuta, :default=>0.0
      String :ABI, :default=>"", :size=>5, :fixed=>true
      String :CAB, :default=>"", :size=>5, :fixed=>true
      String :banca, :default=>"", :size=>50
      String :filiale, :default=>"", :size=>20
      String :contoCorrente, :default=>"", :size=>12, :fixed=>true
      String :IBAN, :default=>"", :size=>27, :fixed=>true
      DateTime :createdAt
      DateTime :updatedAt
      
      index [:anno, :AbbonamentoId], :unique=>true
      index [:anno, :numero], :unique=>true
      index [:dataPagamento]
      index [:ragioneSociale1]
    end
    
    create_table(:schema_info) do
      Integer :version, :default=>0, :null=>false
    end
    
    create_table(:Indirizzo, :ignore_index_errors=>true) do
      primary_key :id
      String :indirizzo, :default=>"", :size=>255
      String :localita, :size=>30
      String :note, :default=>"", :size=>255
      String :CAP, :default=>"", :size=>6, :fixed=>true
      DateTime :createdAt
      DateTime :updatedAt
      foreign_key :ComuneId, :Comune
      
      index [:indirizzo]
    end
    
    create_table(:Cliente, :ignore_index_errors=>true) do
      primary_key :id
      Bignum :numero, :null=>false
      String :ragioneSociale1, :default=>"", :size=>100
      String :ragioneSociale2, :default=>"", :size=>100
      String :codiceFiscale, :default=>"", :size=>16, :fixed=>true
      String :partitaIVA, :default=>"", :size=>11, :fixed=>true
      String :note, :default=>"", :size=>255
      DateTime :createdAt
      DateTime :updatedAt
      foreign_key :IndirizzoFatturaId, :Indirizzo
      foreign_key :IndirizzoId, :Indirizzo
      
      index [:numero]
      index [:ragioneSociale1]
    end
    
    create_table(:AbbonamentoEstintori, :ignore_index_errors=>true) do
      primary_key :id
      Bignum :numero, :null=>false
      String :descrizionePagamento, :default=>"", :size=>20
      String :note, :default=>"", :size=>255
      BigDecimal :costo, :default=>BigDecimal.new("0.0"), :size=>[10, 2]
      Integer :numeroEstintori, :default=>0
      Float :IVA, :default=>0.0
      String :descrizioneSpese, :default=>"", :size=>255
      BigDecimal :costoSpese, :default=>BigDecimal.new("0.0"), :size=>[10, 2]
      TrueClass :seDisdetta, :default=>false
      Integer :giornoEmissione, :default=>1
      Integer :meseEmissione, :default=>1
      Integer :giorniPagamento, :default=>0
      TrueClass :seFatturareAGennaio, :default=>false
      TrueClass :dataFatturaFineMese, :default=>true
      Float :ritenuta, :default=>0.0
      Integer :tipoPagamento, :default=>0
      Integer :tipo, :default=>0
      DateTime :createdAt
      DateTime :updatedAt
      foreign_key :IndirizzoId, :Indirizzo
      foreign_key :ClienteId, :Cliente
      
      index [:ClienteId]
    end
    
    create_table(:ClienteContatto, :ignore_index_errors=>true) do
      DateTime :createdAt
      DateTime :updatedAt
      foreign_key :ClienteId, :Cliente
      foreign_key :ContattoId, :Contatto
      
      index [:ClienteId]
    end
    
    create_table(:ContoCorrente, :ignore_index_errors=>true) do
      primary_key :id
      String :ABI, :default=>"", :size=>5, :fixed=>true
      String :CAB, :default=>"", :size=>5, :fixed=>true
      String :banca, :default=>"", :size=>50
      String :filiale, :default=>"", :size=>20
      String :contoCorrente, :default=>"", :size=>12, :fixed=>true
      String :IBAN, :default=>"", :size=>27, :fixed=>true
      DateTime :createdAt
      DateTime :updatedAt
      foreign_key :ClienteId, :Cliente
      
      index [:ClienteId]
    end
    
    create_table(:AbbonamentoEstintoriContatto, :ignore_index_errors=>true) do
      DateTime :createdAt
      DateTime :updatedAt
      foreign_key :AbbonamentoEstintoriId, :AbbonamentoEstintori
      foreign_key :ContattoId, :Contatto
      
      index [:AbbonamentoEstintoriId]
    end
  end
end
