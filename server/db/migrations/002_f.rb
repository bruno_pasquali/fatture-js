Sequel.migration do
  transaction
  up do
    puts __FILE__
    create_table(:StoricoFattureEstintori) do
    #nota: collegata a tabella indirizzi
      primary_key :id
      Fixnum     :ClienteId, :default => 0
      Fixnum     :AbbonamentoId, :default => 0
      Fixnum     :anno, :null => false, :default => 0    #:ANNOFATT
      Fixnum     :numero, :null => false, :default => 0    #:NUMEFATT
      Fixnum     :numeroPrecedente, :default => 0    #:NUMEPREC
      String     :ragioneSociale1, :size => 100, :default => ""
      String     :ragioneSociale2, :size => 100, :default => ""
      String     :codiceFiscale, :fixed=>true, :size=>16, :default=>""
      String     :partitaIVA, :fixed=>true, :size=>11, :default=>""

      String     :indirizzo, :size => 150, :default => ""
      String     :localita, :size=> 150, :default => ""
      String     :comune, :size=>40, :default => ""
      String     :CAP, :fixed=>true, :size=>6, :default => ""
      String     :provincia, :fixed=>true, :size=>2, :default => ""

      String     :indirizzoFattura, :size => 150, :default => ""
      String     :localitaFattura, :size=> 150, :default => ""
      String     :comuneFattura, :size=>40, :default => ""
      String     :CAPFattura, :fixed=>true, :size=>6, :default => ""
      String     :provinciaFattura, :fixed=>true, :size=>2, :default => ""

      String     :indirizzoManutenzione, :size => 150, :default => ""
      String     :localitaManutenzione, :size=> 150, :default => ""
      String     :comuneManutenzione, :size=>40, :default => ""
      String     :CAPManutenzione, :fixed=>true, :size=>6, :default => ""
      String     :provinciaManutenzione, :fixed=>true, :size=>2, :default => ""

      String     :note, :default=>""       #:NOTE1
      String     :descrizionePagamento, :size=>30, :default=>""    #:PAGAMENT
      # TrueClass  :seRimessaDiretta, :default => false    #:RIMESSAD
      BigDecimal :costo, :size=>[10,2], :default => 0.0       #:COSTO  ---ver. precisione
      Fixnum     :numeroEstintori, :default => 0    #:NUMESTIN
      Float      :IVA, :default => 0.0         #:IVA
      String     :descrizioneSpese, :size => 40, :default=>""    #:DESCSPES
      BigDecimal :costoSpese, :size=>[10,2], :default => 0.0    #:COSTSPES
      DateTime       :dataEmissione, :default => 0    #:DATAEMIS
      DateTime       :dataPagamento, :default => 0    #:DATAPAGA
      TrueClass  :dataFatturaFineMese, :default => true
      TrueClass  :seNotaAccredito, :default => false
      TrueClass  :seDisdetta, :default => false   #:DISDETTA    #---
      Fixnum     :giorniPagamento, :default => 0    #:GGPAGAME
      # TrueClass  :seBonifico, :default => false    #:BONIFICO
      Fixnum     :tipoPagamento, :default => 0
      Float      :ritenuta, :default => 0.0    #:RITENUTA
      String     :ABI, :fixed=>true, :size=>5, :default=>""         #:ABI
      String     :CAB, :fixed=>true, :size=>5, :default=>""         #:CAB
      String     :banca, :size=>50, :default=>""       #:BANCA
      String     :filiale, :size=>20, :default=>""     #:FILIALE
      String     :contoCorrente, :fixed=>true, :size=>12, :default=>""    #:CONTOCOR
      String     :IBAN, :fixed=>true, :size=>27, :default=>""
      DateTime   :createdAt
      DateTime   :updatedAt
      index [:anno, :numero], :unique=>true
      index [:anno, :AbbonamentoId], :unique=>true
      index [:ragioneSociale1]
      index [:dataPagamento]
    end
    create_table(:Cliente) do
      primary_key :id, :type=>Bignum
      Fixnum     :numero, :unique => true, :null => false, :type=>Bignum    #:NUMEFATT
      String     :ragioneSociale1, :size => 100, :default => ""
      String     :ragioneSociale2, :size => 100, :default => ""
      String     :codiceFiscale, :fixed=>true, :size=>16, :default => ""
      String     :partitaIVA, :fixed=>true, :size=>11, :default => ""
      String     :note, :default=>""
      DateTime   :createdAt
      DateTime   :updatedAt
      #Fixnum     :indirizzoFattura
      index [:numero]
      index [:ragioneSociale1]
      foreign_key :IndirizzoFatturaId, :Indirizzo #unico per spezione fattura (condominio)
      foreign_key :IndirizzoId, :Indirizzo #unico per ragione sociale
    end
    create_table(:AbbonamentoEstintori) do
      primary_key :id
      Fixnum     :numero, :unique => true, :null => false, :type=>Bignum    #:NUMEFATT
      # TrueClass  :seRimessaDiretta, :default => false    #:RIMESSAD
      String     :descrizionePagamento, :size=>20, :default=>""    #:PAGAMENT
      String     :note, :default=>""
      BigDecimal :costo, :size=>[10,2], :default=>0.0       #:COSTO  ---ver. precisione
      Fixnum     :numeroEstintori, :default=>0    #:NUMESTIN
      Float      :IVA, :default=>0.0         #:IVA
      String     :descrizioneSpese, :default=>""    #:DESCSPES
      BigDecimal :costoSpese, :size=>[10,2], :default=>0.0    #:COSTSPES
      TrueClass  :seDisdetta, :default=>false    #:DISDETTA    #---
      Fixnum       :giornoEmissione, :default => 1    #:DATAEMIS
      Fixnum       :meseEmissione, :default => 1    #:DATAEMIS
      # DateTime       :dataPagamento, :default => 0    #:DATAEMIS
      Fixnum     :giorniPagamento, :default=>0    #:GGPAGAME
      TrueClass  :seFatturareAGennaio, :default=>false     #:GENNAIO    #---
      TrueClass  :dataFatturaFineMese, :default => true
      # TrueClass  :seBonifico, :default=>false    #:BONIFICO
      Float      :ritenuta, :default=>0.0    #:RITENUTA
      Fixnum     :tipoPagamento, :default => 0
      Fixnum     :tipo, :default => 0    #per comportamenti default: condomini, ..
      DateTime   :createdAt
      DateTime   :updatedAt
      foreign_key :IndirizzoId, :Indirizzo
      foreign_key :ClienteId, :Cliente
      # index [:IndirizzoId]
      index [:ClienteId]
    end
    create_table(:ContoCorrente) do
      primary_key :id
      String     :ABI, :fixed=>true, :size=>5, :default=>""         #:ABI
      String     :CAB, :fixed=>true, :size=>5, :default=>""         #:CAB
      String     :banca, :size=>50, :default=>""       #:BANCA
      String     :filiale, :size=>20, :default=>""     #:FILIALE
      String     :contoCorrente, :fixed=>true, :size=>12, :default=>""    #:CONTOCOR
      String     :IBAN, :fixed=>true, :size=>27, :default=>""
      DateTime   :createdAt
      DateTime   :updatedAt
      foreign_key :ClienteId, :Cliente
      index [:ClienteId]
#      index [:ABI, :CAB]
#      index [:banca, :filiale]
    end
    create_table(:Indirizzo) do
      primary_key :id
      String     :indirizzo, :default => ""
      String     :localita, :size=>30
      String     :note, :default=>""
      String     :CAP, :fixed=>true, :size=>6, :default => ""
      DateTime   :createdAt
      DateTime   :updatedAt
      foreign_key :ComuneId, :Comune
      index [:indirizzo]
    end
    create_table(:Contatto) do
      primary_key :id
      String     :contatto, :size=>255, :default=>""
      String     :descrizione, :size=>50, :default=>""
      Fixnum     :tipo, :default => 0
      DateTime   :createdAt
      DateTime   :updatedAt
    end
    create_table(:ClienteContatto) do
      DateTime   :createdAt
      DateTime   :updatedAt
      foreign_key :ClienteId, :Cliente
      foreign_key :ContattoId, :Contatto
      index [:ClienteId] #, :ContattoId]
    end
    create_table(:AbbonamentoEstintoriContatto) do
      DateTime   :createdAt
      DateTime   :updatedAt
      foreign_key :AbbonamentoEstintoriId, :AbbonamentoEstintori
      foreign_key :ContattoId, :Contatto
      index [:AbbonamentoEstintoriId] #, :ContattoId]
    end
  end
  down do
    def downAction table
      puts "Dropping: " + table.to_s
      self.drop_table table
    end
    puts __FILE__
    downAction :StoricoFattureEstintori
    downAction :AbbonamentoEstintoriContatto
    downAction :ClienteContatto
    downAction :AbbonamentoEstintori
    downAction :ContoCorrente
    downAction :Cliente
    downAction :Indirizzo
    downAction :Contatto
  end
end
