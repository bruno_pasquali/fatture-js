Sequel.migration do
	up do
    	create_or_replace_view(:clientiView,
			"SELECT Cliente.id, Cliente.numero,
                    Cliente.ragioneSociale1, Cliente.ragioneSociale2,
                    Indirizzo.indirizzo, Indirizzo.localita,
                    Comune.comune
			   FROM Cliente
			   LEFT OUTER JOIN Indirizzo
			     ON Cliente.indirizzoId = Indirizzo.id
			       LEFT OUTER JOIN Comune
			         ON Indirizzo.ComuneId = Comune.id;")
	end
	down do
		drop_view(:clientiView)
	end
end
