//
//Automatic generated file: do not modify directly
//
module.exports = function(sequelize, DataTypes) {
    return sequelize.define('ContoCorrente', {
        ABI:{ type: 'char(5)' , defaultValue: '' },
        CAB:{ type: 'char(5)' , defaultValue: '' },
        banca:{ type: 'varchar(50)' , defaultValue: '' },
        filiale:{ type: 'varchar(20)' , defaultValue: '' },
        contoCorrente:{ type: 'char(12)' , defaultValue: '' },
        IBAN:{ type: 'char(27)' , defaultValue: '' },
    }, {freezeTableName: true});
};