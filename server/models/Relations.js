module.exports.injectRelations = function(m, sequelize, DataTypes) {
// CREATE TABLE `AbbonamentoEstintoriContatto` 
// (`ContattoId` INTEGER NOT NULL, PRIMARY KEY (`AbbonamentoEstintoriId`, `ContattoId`))

    // m.AbbonamentoEstintoriContatto = sequelize.define('AbbonamentoEstintoriContatto', {
    //     status: DataTypes.STRING
    // })
    // describe relationships
    m.Cliente
        .hasMany(m.AbbonamentoEstintori/*, {foreignKeyConstraint: true, through: LeftsRigths }*/)
        .hasMany(m.ContoCorrente/*, {foreignKeyConstraint: true, through: LeftsRigths }*/)
        .belongsTo(m.Indirizzo/*, {foreignKeyConstraint: true, through: LeftsRigths }*/)
        .belongsTo(m.Indirizzo, 
            { 
                as: 'IndirizzoFattura' ,
                foreignKey: 'IndirizzoFatturaId'
                //, onDelete: 'cascade', onUpdate: 'restrict'
            }
        )
        .hasMany(m.Contatto/*, {foreignKeyConstraint: true, through: LeftsRigths }*/);

    m.ContoCorrente
        .belongsTo(m.Cliente/*, {foreignKeyConstraint: true, through: LeftsRigths }*/);

    m.Indirizzo
        .hasOne(m.AbbonamentoEstintori/*, {foreignKeyConstraint: true, through: LeftsRigths }*/)
        .hasOne(m.Cliente/*, {foreignKeyConstraint: true, through: LeftsRigths }*/)
        .belongsTo(m.Comune/*, {foreignKeyConstraint: true, through: LeftsRigths }*/);

    m.AbbonamentoEstintori
        .belongsTo(m.Cliente/*, {foreignKeyConstraint: true, through: LeftsRigths }*/)
        .belongsTo(m.Indirizzo/*, {foreignKeyConstraint: true, through: LeftsRigths }*/)
        .hasMany(m.Contatto/*, {foreignKeyConstraint: true, through: LeftsRigths }*/);

    m.Comune
        .hasMany(m.Indirizzo/*, {foreignKeyConstraint: true, through: LeftsRigths }*/);

    m.Contatto
        .hasMany(m.Cliente/*, {foreignKeyConstraint: true, through: LeftsRigths }*/)
        .hasMany(m.AbbonamentoEstintori/*, {foreignKeyConstraint: true, through: LeftsRigths }*/);
};
