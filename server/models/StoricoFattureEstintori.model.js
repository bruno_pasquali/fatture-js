//
//Automatic generated file: do not modify directly
//
module.exports = function(sequelize, DataTypes) {
    return sequelize.define('StoricoFattureEstintori', {
        ClienteId:{ type: 'integer' , defaultValue: 0 },
        AbbonamentoId:{ type: 'integer' , defaultValue: 0 },
        anno:{ type: 'integer' , allowNull: false, defaultValue: 0 },
        numero:{ type: 'integer' , allowNull: false, defaultValue: 0 },
        numeroPrecedente:{ type: 'integer' , defaultValue: 0 },
        ragioneSociale1:{ type: 'varchar(100)' , defaultValue: '' },
        ragioneSociale2:{ type: 'varchar(100)' , defaultValue: '' },
        codiceFiscale:{ type: 'char(16)' , defaultValue: '' },
        partitaIVA:{ type: 'char(11)' , defaultValue: '' },
        indirizzo:{ type: 'varchar(150)' , defaultValue: '' },
        localita:{ type: 'varchar(150)' , defaultValue: '' },
        comune:{ type: 'varchar(40)' , defaultValue: '' },
        CAP:{ type: 'char(6)' , defaultValue: '' },
        provincia:{ type: 'char(2)' , defaultValue: '' },
        indirizzoFattura:{ type: 'varchar(150)' , defaultValue: '' },
        localitaFattura:{ type: 'varchar(150)' , defaultValue: '' },
        comuneFattura:{ type: 'varchar(40)' , defaultValue: '' },
        CAPFattura:{ type: 'char(6)' , defaultValue: '' },
        provinciaFattura:{ type: 'char(2)' , defaultValue: '' },
        indirizzoManutenzione:{ type: 'varchar(150)' , defaultValue: '' },
        localitaManutenzione:{ type: 'varchar(150)' , defaultValue: '' },
        comuneManutenzione:{ type: 'varchar(40)' , defaultValue: '' },
        CAPManutenzione:{ type: 'char(6)' , defaultValue: '' },
        provinciaManutenzione:{ type: 'char(2)' , defaultValue: '' },
        note:{ type: 'varchar(255)' , defaultValue: '' },
        descrizionePagamento:{ type: 'varchar(30)' , defaultValue: '' },
        costo:{ type: 'numeric(10, 2)' , defaultValue: 0.0 },
        numeroEstintori:{ type: 'integer' , defaultValue: 0 },
        IVA:{ type: 'double precision' , defaultValue: 0.0 },
        descrizioneSpese:{ type: 'varchar(40)' , defaultValue: '' },
        costoSpese:{ type: 'numeric(10, 2)' , defaultValue: 0.0 },
        dataEmissione:{ type: 'timestamp' , defaultValue: 0 },
        dataPagamento:{ type: 'timestamp' , defaultValue: 0 },
        dataFatturaFineMese:{ type: DataTypes.BOOLEAN , defaultValue: 1 },
        seNotaAccredito:{ type: DataTypes.BOOLEAN , defaultValue: 0 },
        seDisdetta:{ type: DataTypes.BOOLEAN , defaultValue: 0 },
        giorniPagamento:{ type: 'integer' , defaultValue: 0 },
        tipoPagamento:{ type: 'integer' , defaultValue: 0 },
        ritenuta:{ type: 'double precision' , defaultValue: 0.0 },
        ABI:{ type: 'char(5)' , defaultValue: '' },
        CAB:{ type: 'char(5)' , defaultValue: '' },
        banca:{ type: 'varchar(50)' , defaultValue: '' },
        filiale:{ type: 'varchar(20)' , defaultValue: '' },
        contoCorrente:{ type: 'char(12)' , defaultValue: '' },
        IBAN:{ type: 'char(27)' , defaultValue: '' },
    }, {freezeTableName: true});
};