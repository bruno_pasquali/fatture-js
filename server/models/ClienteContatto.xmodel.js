
// Automatic generated file: do not modify directly

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('ClienteContatto', {
        ClienteId:{ type: 'integer' , references: 'Cliente', referencesKey: 'id' },
        ContattoId:{ type: 'integer' , references: 'Contatto', referencesKey: 'id' },
    }, {freezeTableName: true});
};