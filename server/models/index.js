var Sequelize = require('sequelize'),
    fs = require('fs'),
    path = require('path'),
    util = require('util'),
    config = require('../config');

if (fs.exists('logs/sequelize.log')) fs.unlinkSync('logs/sequelize.log');
var log = function(data) {
    fs.appendFileSync('logs/sequelize.log', data + '\n');
    // console.log(util.inspect(data, utilOptions));
};

var options = config.db.options;
options.define = {
    instanceMethods: {
        toPlainObject: function(readOnly) {
            return this.injectModelInto(JSON.parse(JSON.stringify(this)), readOnly);
        },
        injectModelInto: function(obj, readOnly) {
            var self = this;
            obj._bag = {
                model: self.daoFactoryName,
                readOnly: Boolean(readOnly)
            };
            for (var p in self.dataValues) {
                if (util.isArray(self.dataValues[p])) {
                    var i = 0,
                        l = self.dataValues[p].length;
                    for (; i < l; i++) {
                        self.dataValues[p][i].injectModelInto(obj[p][i], readOnly);
                    }

                } else if ((self.dataValues[p] === Object(self.dataValues[p]) &&
                    (self.dataValues[p]['dataValues']))) {
                    self.dataValues[p].injectModelInto(obj[p], readOnly);
                }
            }
            return obj;
        }
    }
};
options.logging = log;
options.language = 'xx'; //no pluralization
var sequelize = new Sequelize(
    config.db.dataBase,
    config.db.username,
    config.db.password,
    options
);

// by suggestions from:
// http://redotheweb.com/2013/02/20/sequelize-the-javascript-orm-in-practice.html
var fileNameEndings = {
    '.model.js': (function(fileName, extext, models) {
        models[path.basename(fileName, extext)] =
            sequelize.import(fileName);
    }),
    '.helper.js': (function(fileName, extext, models) {
        models[path.basename(fileName, extext)] =
            require(path.join(__dirname, fileName)).init(models);
    }),
    '.models.js': (function(fileName, extext, models) {
        var loadedModels = require(path.join(__dirname, fileName));
        for (var p in loadedModels) {
            if (loadedModels.hasOwnProperty(p)) {
                models[p] = loadedModels[p](sequelize, Sequelize);
            }
        }
    }),
    //only syntax check
    // '.xhelper.js': (function(fileName, extext, models) {
    //     require(path.join(__dirname, fileName));
    // }),
    '.xmodels.js': (function(fileName, extext, models) {
        require(path.join(__dirname, fileName));
    }),
    '.xmodel.js': (function(fileName, extext, models) {
        require(path.join(__dirname, fileName));
    })
};
(function(m) {
    fs.readdirSync(__dirname).forEach(function(fileName) {
        var extext = path.extname(path.basename(fileName, path.extname(fileName))) + path.extname(fileName);
        if (extext in fileNameEndings) {
            fileNameEndings[extext](fileName, extext, m);
        }
    });
    require(path.join(__dirname, 'Relations')).injectRelations(m);
})(module.exports);

// export connection
module.exports.sequelize = sequelize;
module.exports.Sequelize = Sequelize;
module.exports.utils = Sequelize.Utils;
