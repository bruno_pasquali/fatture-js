var _m = null;

var _find = function(id){
    return _m.AbbonamentoEstintori.find({
    where : {id : id},
    include: [
        _m.Contatto,
        {
            model: _m.Indirizzo,
            include: [_m.Comune]
        }
    ]});
};

_destroy = function(query) {
    return new _m.utils.CustomEventEmitter(function(emitter) {
        _find(query.id)
        .ok(function(item){
            var rel_chain = null,
                del_chain = null;
            if (!item) {
                return emitter.emit('success');
            }
            rel_chain = new _m.utils.QueryChainer();
            rel_chain.add(item.setContatto([]));
            if (item.indirizzo) {
                rel_chain.add(item.setIndirizzo(null));
            }
            rel_chain.run()
            .ok(function(){
                del_chain = new _m.utils.QueryChainer();
                if (item.indirizzo) {
                    del_chain.add(item.indirizzo.destroy());
                }
                item.contatto.map(function(item) {
                    del_chain.add(item.destroy());
                });
                del_chain.add(item.destroy());
                del_chain.run()
                .ok(function(){
                    emitter.emit('success');
                }).error(function (error) {
                    emitter.emit('error', error);
                });
            }).error(function (error) {
                emitter.emit('error', error);
            });
        }).error(function (error) {
            emitter.emit('error', error);
        });
    }).run();
};

_getTableName = function() { return 'abbonamenti' };
module.exports.init = function(models){
    _m = models;
    return {
        name: _getTableName(),
        getTableName: _getTableName,
        find: _find,
        destroy: _destroy
    }
};
