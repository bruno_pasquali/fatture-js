//
//Automatic generated file: do not modify directly
//
module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Contatto', {
        contatto:{ type: 'varchar(255)' , defaultValue: '' },
        descrizione:{ type: 'varchar(50)' , defaultValue: '' },
        tipo:{ type: 'integer' , defaultValue: 0 },
    }, {freezeTableName: true});
};