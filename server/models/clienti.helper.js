var _m = null;

var _findAll = function(query){
	query.include = [{model: _m.Indirizzo, include: [_m.Comune]}];
	return _m.Cliente.findAll(query);
};

var _find = function(id) {
	return _m.Cliente.find({
		where : {id : id},
		include : [
			_m.Contatto,
			_m.ContoCorrente,
			{
				model: _m.Indirizzo,
				include: [_m.Comune]
			},
			{
				model : _m.Indirizzo,
				as : 'IndirizzoFattura',
				include: [_m.Comune]
			}, {
				model: _m.AbbonamentoEstintori,
				include: [
					_m.Contatto,
					{
						model: _m.Indirizzo,
						include: [_m.Comune]
					}
				]
			}
	]});
};


_destroy = function(query) {
	return new _m.utils.CustomEventEmitter(function(emitter) {
		_find(query.id)
		.ok(function(item){
		    var rel_chain = null,
		    	del_chain = null;
			if (!item) {
			    return emitter.emit('success');
			}
			// if (item.abbonamentoEstintori && item.abbonamentoEstintori['length']) {
			// 	return emitter.emit('error', 'Non è possibile cancellare un Cliente con abbonamenti');
			// }
		    rel_chain = new _m.utils.QueryChainer();
			rel_chain.add(item.setContatto([]));
            rel_chain.add(item.setContoCorrente([]));
            rel_chain.add(item.setAbbonamentoEstintori([]));
		    if (item.indirizzo) {
				rel_chain.add(item.setIndirizzo(null));
			}
		    if (item.indirizzoFattura) {
				rel_chain.add(item.setIndirizzoFattura(null));
			}
			rel_chain.run()
		    .ok(function(){
			    del_chain = new _m.utils.QueryChainer();
			    if (item.indirizzo) {
					del_chain.add(item.indirizzo.destroy());
			    }
			    if (item.indirizzoFattura) {
					del_chain.add(item.indirizzoFattura.destroy());
			    }
			    item.contatto.map(function(item) {
			    	del_chain.add(item.destroy());
			    });
			    item.contoCorrente.map(function(item) {
			    	del_chain.add(item.destroy());
			    });

                item.abbonamentoEstintori.map(function(item) {
                    del_chain.add(_m.abbonamenti.destroy({id: item.id}));
                });

		    	del_chain.add(item.destroy());
			    del_chain.run()
			    .ok(function(){
			    	emitter.emit('success');
				}).error(function (error) {
					emitter.emit('error', error);
			    });
			}).error(function (error) {
				emitter.emit('error', error);
		    });
		}).error(function (error) {
			emitter.emit('error', error);
		});
	}).run();
};

_getTableName = function() { return 'clienti' };
module.exports.init = function(models){
	_m = models;
	return {
        name: _getTableName(),
        getTableName: _getTableName,
		find: _find,
		findAll: _findAll,
		destroy: _destroy
	}
};
