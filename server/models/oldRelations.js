module.exports.injectRelations = function(m) {
    // describe relationships
    m.Cliente
    .hasMany(m.AbbonamentoEstintori, {
        foreignKey: 'ClienteId'
    })
    .hasMany(m.ContoCorrente, {
        foreignKey: 'ClienteId'
    })
    .belongsTo(m.Indirizzo, {
        foreignKey: 'IndirizzoId'
    })
    .belongsTo(m.Indirizzo, {
        as: 'IndirizzoFattura',
        foreignKey: 'IndirizzoFatturaId'
    })
    .hasMany(m.Contatto, {
        foreignKey: 'ClienteId',
        // joinTableName: 'ClienteContatto'
    });

    m.ContoCorrente
    .belongsTo(m.Cliente, {
        foreignKey: 'ClienteId'
    });

    m.Indirizzo
    .hasOne(m.AbbonamentoEstintori, {
        foreignKey: 'IndirizzoId'
    })
    .hasOne(m.Cliente, {
        foreignKey: 'IndirizzoId'
    })
    .belongsTo(m.Comune, {
        foreignKey: 'ComuneId'
    });

    m.AbbonamentoEstintori
    .belongsTo(m.Cliente, {
        foreignKey: 'ClienteId'
    })
    .belongsTo(m.Indirizzo, {
        foreignKey: 'IndirizzoId'
    })
    .hasMany(m.Contatto, {
        foreignKey: 'AbbonamentoEstintoriId',
        // joinTableName: 'AbbonamentoEstintoriContatto'
    });

    m.Comune
    .hasMany(m.Indirizzo, {
        foreignKey: 'ComuneId'
    });

    m.Contatto
    .hasMany(m.Cliente, {
        foreignKey: 'ContattoId',
        // joinTableName: 'ClienteContatto'
    })
    .hasMany(m.AbbonamentoEstintori, {
        foreignKey: 'ContattoId',
        // joinTableName: 'AbbonamentoEstintoriContatto'
    });
};
