//
//Automatic generated file: do not modify directly
//
module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Comune', {
        istat:{ type: 'integer'  },
        comune:{ type: 'varchar(40)'  },
        CAP:{ type: 'char(6)'  },
        provincia:{ type: 'char(2)'  },
        regione:{ type: 'char(3)'  },
        prefisso:{ type: 'char(4)'  },
        codiceFisco:{ type: 'char(4)'  },
    }, {freezeTableName: true});
};