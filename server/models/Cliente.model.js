//
//Automatic generated file: do not modify directly
//
module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Cliente', {
        numero:{ type: 'bigint' , allowNull: false },
        ragioneSociale1:{ type: 'varchar(100)' , defaultValue: '' },
        ragioneSociale2:{ type: 'varchar(100)' , defaultValue: '' },
        codiceFiscale:{ type: 'char(16)' , defaultValue: '' },
        partitaIVA:{ type: 'char(11)' , defaultValue: '' },
        note:{ type: 'varchar(255)' , defaultValue: '' },
    }, {freezeTableName: true});
};