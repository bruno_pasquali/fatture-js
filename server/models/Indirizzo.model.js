//
//Automatic generated file: do not modify directly
//
module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Indirizzo', {
        indirizzo:{ type: 'varchar(255)' , defaultValue: '' },
        localita:{ type: 'varchar(30)'  },
        note:{ type: 'varchar(255)' , defaultValue: '' },
        CAP:{ type: 'char(6)' , defaultValue: '' },
    }, {freezeTableName: true});
};