//
//Automatic generated file: do not modify directly
//
module.exports = function(sequelize, DataTypes) {
    return sequelize.define('AbbonamentoEstintoriContatto', {
        AbbonamentoEstintoriId:{ type: 'integer' , references: 'AbbonamentoEstintori', referencesKey: 'id' },
        ContattoId:{ type: 'integer' , references: 'Contatto', referencesKey: 'id' },
    }, {freezeTableName: true});
};