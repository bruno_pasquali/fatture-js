//
//Automatic generated file: do not modify directly
//
module.exports = function(sequelize, DataTypes) {
    return sequelize.define('AbbonamentoEstintori', {
        numero:{ type: 'bigint' , allowNull: false },
        descrizionePagamento:{ type: 'varchar(20)' , defaultValue: '' },
        note:{ type: 'varchar(255)' , defaultValue: '' },
        costo:{ type: 'numeric(10, 2)' , defaultValue: 0.0 },
        numeroEstintori:{ type: 'integer' , defaultValue: 0 },
        IVA:{ type: 'double precision' , defaultValue: 0.0 },
        descrizioneSpese:{ type: 'varchar(255)' , defaultValue: '' },
        costoSpese:{ type: 'numeric(10, 2)' , defaultValue: 0.0 },
        seDisdetta:{ type: DataTypes.BOOLEAN , defaultValue: 0 },
        giornoEmissione:{ type: 'integer' , defaultValue: 1 },
        meseEmissione:{ type: 'integer' , defaultValue: 1 },
        giorniPagamento:{ type: 'integer' , defaultValue: 0 },
        seFatturareAGennaio:{ type: DataTypes.BOOLEAN , defaultValue: 0 },
        dataFatturaFineMese:{ type: DataTypes.BOOLEAN , defaultValue: 1 },
        ritenuta:{ type: 'double precision' , defaultValue: 0.0 },
        tipoPagamento:{ type: 'integer' , defaultValue: 0 },
        tipo:{ type: 'integer' , defaultValue: 0 },
    }, {freezeTableName: true});
};