/**
 * Module dependencies.
 */
var express = require('express')
  , routes = require('./routes')
  // , fscrud = require('./routes/fscrud')
  , crud = require('./routes/crud')
  , rest = require('./routes/rest')
  , http = require('http')
  , path = require('path')
  , fs = require('fs');

if (! fs.existsSync('logs')) fs.mkdirSync('logs');

var models = require('./models');
var app = express();

// my settings
app.set('models', models);
app.set('utils', app.get('models').utils);
var config = require('./config');
app.set('config', config);
app.set('port', process.env.PORT || config.PORT);
app.set('ip', process.env.IP || config.IP);

// all environments
app.use(express.logger('dev'));
app.use(express.compress());
app.use(express.methodOverride());

if ('development' == config.env) {
  app.use(function(req, res, next) {req.rawBody = '';
    // req.setEncoding('utf8');
    req.on('data', function(chunk) { req.rawBody += chunk; });
    next();
  });
}
app.use(express.bodyParser());
// app.use(express.basicAuth(function(user, pass){
// return 'bruno' == user & '' == pass;
// }));

app.use(rest.parseQuery);

app.use(app.router);
app.use(express.static('public'));
app.use(express.static(path.join(__dirname, 'public', 'bower_components')));
//for jasmine spec runner
app.use(express.static(path.join(__dirname, '..', 'clientSpec')));
//app.use('/fs', express.static(path.join(__dirname, '..', 'client', 'app', 'fs')));
//app.use('/fs', express.directory(path.join(__dirname, '..', 'client', 'app', 'fs')));
//app.use(express.favicon('favicon.ico'));

// development only
if ('development' == config.env) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);


// app.all('/r/*', function (req, res, next) {
//   if(req.path.slice(-4).toLowerCase() === '.csv') {
//     console.log(req.path.slice(-4).toLowerCase());
//     req.format_csv = true;
//     req.url = req.url.substr(0, req.path.length-4)
//     console.log(req.route);
//   }
//   next();
// });

app.param('table', function (req, res, next, table) {
  rest.checkTableParam(req, res, table)
  || next();
});

app.param('subTable', function (req, res, next, subTable) {
  var a = subTable.split('.');
  if (a.length < 2) { return next(); }
  req.params.subAlias = a[1];
  req.params.subTable = a[0];
  rest.checkTableParam(req, res, req.params.subTable)
  || next();
});

// app.get('/fs/:name', fscrud.item);
// app.post('/fs/:name', fscrud.update);
// app.put('/fs/:name', fscrud.update);
// app.delete('/fs/:name', fscrud.destroy);

// app.get('/r/clientiView', crud.list);
//TODO: regexp
var addTableParam = function (module, table, verb) {
  return function (req, res, next) {
    req.params.table = table;
    require('./routes/' + module)[verb](req, res, next);
  };
};
var addSubTableParam = function (module, table, verb) {
  return function (req, res, next) {
    req.params.subTable = table;
    require('./routes/' + module)[verb](req, res, next);
  };
};

app.get('/s/:table', crud.schema);
app.get('/s/', crud.tables);

app.get('/r/clientiView', addTableParam('clientiView', 'clientiView', 'list'));
app.get('/r/:table', crud.list);

// app.get('/r/clienti/:id', addTableParam('clienti', 'clienti', 'item'));
// app.get('/r/abbonamenti/:id', addTableParam('abbonamenti', 'abbonamenti', 'item'));
app.get('/r/:table/:id', crud.item);
app.get('/r/:table/:id/:subTable', crud.subListOrItem);

app.post('/r/Cliente', addTableParam('conNumeroUnico', 'Cliente', 'create'));
app.post('/r/StoricoFattureEstintori',
         addTableParam('conNumeroAnnoUnico', 'StoricoFattureEstintori', 'create'));
app.post('/r/AbbonamentoEstintori',
         addTableParam('conNumeroUnico', 'AbbonamentoEstintori', 'create'));
app.post('/r/:table/:id/AbbonamentoEstintori',
         addSubTableParam('conNumeroUnico', 'AbbonamentoEstintori', 'subCreate'));
app.post('/r/:table', crud.create);
app.post('/r/:table/:id/:subTable', crud.subCreate);

app.put('/r/:table/:id', crud.update);
app.put('/r/:table', crud.deepUpdate);

if (process.env.NODE_ENV === 'test') {
  app.delete('/r/all', crud.vacuum);
}
app.delete('/r/:table/:id', crud.destroy);
app.delete('/r/:table/:id/:subTable/:subId', crud.subDestroy);


var syncParameters = (process.env.NODE_ENV === 'test') ? {force: true}: {force: false};
//var syncParameters = {force: false};
app.get('models').sequelize.sync(syncParameters).complete(function (err) {
  if (err) { throw err; }
  console.log('Process PORT / IP:' + process.env.IP + " / " + process.env.PORT);
  var server = http.createServer(app).listen(app.get('port'), app.get('ip'), function () {
    console.log('Express server listening at ' + console.dir(server.address()));
    console.log('env: '+ console.dir(config));
    console.log('Resetting tables: '+ syncParameters.force);
    console.log('working directory: ' + process.cwd());
  });
});
