@ECHO OFF
rem CD ..
setlocal
IF EXIST %ProgramFiles(x86)%\Git (
	SET GITPATH="%ProgramFiles(x86)%\Git\bin\sh.exe" 
) ELSE (
	SET GITPATH="%ProgramFiles%\Git\bin\sh.exe" 
)
cmd.exe /c %GITPATH% --login -i %*